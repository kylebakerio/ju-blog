---
title: "3 Months to Win: The Whitebelt Tournament"
tags:
  - guillotine
  - sprawl
  - takedowns
  - standup
  - white belt
  - tournament
  - competition
  - beginner
mp3: 'http://ju.kylebaker.io/music/flowing-water.mp3'
cover: '/img/oakley/comp-1-2-177-1600.jpg'
date: 2019-05-26 14:38:45
---

Around February 2019, I started teaching across town twice a week at a small fitness gym, Oakley Fitness Center. They wanted beginner's no-gi BJJ. Right from the start we had a core group of three guys, ranging from their 30's to their 40's, who showed up to both classes every week. All of them had some kind of minimal experience--one had done a few months of BJJ a year ago, another had some wrestling in his past and had jumped into a submission tournament randomly years ago, and the last had trained at an MMA gym nearly 15 years ago in another life for a bit.

![](/img/oakley/group-1.jpg)

We started from the beginning. Because it was a small group and everyone attended every class, the structure looked more like a private lesson. It was direct, hands-on, with a close attention to detail on every drill execution and a conversational style that you can't get in a larger class. We worked all the basic positions, a few transition details, looked at basic submission sequences from those positions, at armbar/RNC/triangle finishing details, dealing with closed guard, a handful of sweeps and when to use them. We even touched on straight ankle locks and kesa gatame. We did positional sparring every class.

![](/img/oakley/train-1.jpg)

For two months, we fit a lot in, and progress was tangible and swift. Then, at about that time, the guys all agreed they were game to enter into the no-gi division of the 3rd Asheville Whitebelt Tournament, a small local tournament hosted at Open Source BJJ.

The rules would be basically submission-only with EBI-style overtime. Kneebars and straight-anklelocks allowed, no heel-hooks or toe-holds.

So, with one month left, we focused exclusively on competition prep. I decided rather than try to improve their overall game, which is a slow process that takes time, the highest returns were likely to be in two places--the beginning of the match, and the overtime end.

![](/img/oakley/train-3.jpg)

Two of the students had a wrestling background, one did not. For the one without a wrestling background, I told him his time was far better spent learning to bait shots, sprawl effectively, and guillotine. Doing wrestling well is harder than most people think, and sprawling and guillotining are much more easily acquired skills. A counter-based game is a much more time-efficient route, since we had one month to work with.

![](/img/oakley/train-2.jpg)

Perfectly, since the other students _did_ have a wrestling background, I had them primarily focus on cleaning up their shots and learning to protect their neck, making the modifications necessary to their wrestling shots to avoid guillotines.

The rest of the time was spent on EBI overtime drills. We focused on the obvious topics of escape techniques and concepts, control concepts, finishing techniques and concepts for back and arm. But we also talked about the mental differences. I drilled into their mind a simple algorithm when on top and on bottom. I taught them to focus on speed in overtime instead of being patient, to prioritize control instead of being flexible to upgrade and accept transitions, to refresh in their own mind exactly what they needed to attempt in the moments _before_ the overtime round would start.

Competition day came... There are no age divisions in a small local tournament, and weight classes were set on the day-off. Everyone had a round-robin division with about 4-5 other people.

And the results were better than I could have hoped for. Three guys, four medals. One guy got double silvers (absolute and weight), another guy got gold in his division, and guy #3 also got silver. They nailed the game-plan, making the stand-up game run according to their goals, finishing several guillotines. And they clearly excelled in the overtime format.

![](/img/oakley/comp-1-9.jpg)
![](/img/oakley/comp-1-4.jpg)
![](/img/oakley/comp-1-3.jpg)
![](/img/oakley/comp-1-10.jpg)

This felt like a special win for me, because it's my first time giving hand-guided, self-directed prep for specific students, with a custom game-plan. Seeing that manifest and work in action? I'm pretty proud of these guys. I never thought a white belt tournament would mean so much to me, but once again I find myself surprised.

Way to go, guys.

![](/img/oakley/comp-1-12.jpg)


