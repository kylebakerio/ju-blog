---
title: "Judo Shodan to Nidan: What's the difference?"
tags:
  - judo
  - shodan
  - nidan
  - black belt
  - promotion
  - history
  - competition
mp3: 'http://ju.kylebaker.io/music/the-midnight-cackdaws-crying.mp3'
cover: '/img/travis-kano-nidan-gimp-crop.jpg'
date: 2019-02-22 12:31:32
---

<!-- add relevant instagram content as needed -->

To the uninitiated, hearing someone with a black belt say that they "have so much to learn" and are "just at the beginning of their journey" can sound... disingenuous, to say the least. Is this false humility? Or have they just lost sight? Surely all that remains for them is to polish little details?

Well, I have some data for you.

Approximately 1 year and 11 months after receiving my shodan, my nidan rank was confirmed.

Which means it has been about two years as a black belt in Judo. And I honestly didn't see it coming, but... I really do feel like I grew a lot in that time.

Since shodan, I have:

- had 21 competition matches in 6 divisions
- travelled through Europe training and teaching at various Judo and BJJ gyms and locations
- continued assisting with teaching at my home gym while I was in town
- somehow even ended up being asked to and founding a small Judo club here in NC and began teaching regularly

At some level I knew I still had so much to learn (attempting to perform the _nage-no-kata_ can be a humbling process), but I didn't really believe I would see myself transform as much as I did in that time.

In an attempt to be less philosophical, and more concrete, I think I'd say there are at least a handful of clear differences I can point to.

As a brown belt, I competed at the Dallas Invitational (one of the "Big Three" Judo comps in the US), tying for 5th in a 17 man senior elite bracket. It was in part based on my competition performance that season that I was promoted to _shodan_ as soon as I was. I was very proud of that performance, but at the same time, those losses stuck with me...

My two losses were to the two who placed 1st and 2nd in that division (1st place was Jack Hatton). Earlier that season, I also lost a match to another international competitor, Arthur "Kyle" Wright, at another competition, getting me a silver. (I didn't face him at the Dallas Invitational, but he got 4th in that competition.)

Facing those two, I was left with a very strong impression about a difference in my game and theirs. It was something that seemed so simple.

<b>It was their grips.</b>

I was unable to play my game, because their gripping game was so good. Both of those matches, I basically lost to shidos, because I could do nothing offensively while being so outgripped. This was completely unfamiliar to me, and at the time I didn't have an answer to that.

I took that to heart. It has changed my Judo, and made it so much easier. Frequently, other players are oblivious to what I'm doing, and it is astounding how one-sided it makes the match when one player can outgrip the other.

Now as a teacher, I constantly emphasize grip-fighting, drill grip-fighting, and coach grip-fighting. It's been an incredible difference. 

<b>The next thing I recall</b> is, shortly after my promotion to shodan, being asked to sit alongside one of my instructors and assist in grading the <span title="those below black belt grade">_mudansha_</span>.

They would demonstrate a technique. My instructor would look at me and ask me to comment on it.

I felt like I choked half the time. I'd know something was off, but I couldn't explain it. It just felt wrong. Judo is fast, and it's subtle. It's complex. It has a bunch of moving parts, and it starts and stops in a fraction of a second. It's dynamic, and moves look different from different people with opponents of different body types.

Pinpointing what a technique feels 'off' when you see it? I had just gotten to the place where I had a reasonable sense of what felt 'right' and what felt 'wrong'. But that day, as I watched my instructor critique the particular details of their techniques, I gained a completely new appreciation for his skill.

I felt completely outclassed. Embarassed, a little, even.

That's changed, though. And honestly, that's probably been the most satisfying evolution to experience. Teaching Judo has become an extremely rich experience for me, because I see a move executed, and I _understand_ what just happened. I understand why and how it works, how they are doing it, why they are doing it, and how to explain the difference between their execution and the ideal they strive for. I feel like I've grown leaps and bounds in this area.

And yet? Yep. I can feel it. There's still more. There are still times, still moves, where I can't put my finger on it.

That's ok. These things have a way of sorting themselves out.

But it is just immensely satisfying to _know_ a thing at this level, to be able to be helpful in this way.

<b>The last would be a question of technique.</b> Technique progress can be hard to gauge--it usually is, for me, very difficult to put my finger on it. But one move in particular has been my yardstick for the last two years:

<b>Uchi-Mata</b>

When I was told that I should begin studying the <span title="the one kata every judo black belt has to know some or all of">_nage-no-kata_</span> to prepare for my _shodan_, I initially thought the hardest part would just be the rote memorization.

I was wrong.

Suddenly, techniques you thought you could do just fine make you feel like an idiot. Doing them that smoothly, that cleanly, all of them, every time, on display, perfectly? Ouch.

Also, I expected it to be boring. Really boring. Isn't kata always boring? I'd never done _kata_ before, but I had the typical disdain for it.

I'll have to write more about it in another post, but _kata_ was actually enlightening. Without derailing into that topic too much, one throw in particular really made me nervous... _uchi-mata_.

<!-- insert vid here -->

I barely felt halfway decent at the throw on its own, but in the _nage-no-kata_ form, you have to do it in a very particular way off of a rotating entry.

I practiced it probably more than any of the other throws. And, through that process, actually came to really understand the throw--truly--for the first time. It _clicked_.

That was super exciting, but that was just the beginning. Two years later, I've hit that move in competition, I hit it regularly in randori, and I feel now that I can teach it quite well. It's become one of my absolute favorite techniques.

I've grown with other techniques as well, but none has been quite so transparent to me as _uchi-mata_, which has been a very cool move to have.

Which makes all the more clear that there is future room for growth still... there are still moves I don't feel a really deep grasp of, to study and learn deeply. ¯\\\_(ツ)\_/¯

There have been other areas of growth--there always is more than we know--but I think these are likely the more interesting changes that I wouldn't have really guessed in a meaningful way before I got here.

I'm looking forward to the path to _sandan_. Maybe there's always more to learn after all.
