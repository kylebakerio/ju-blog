---
title: "Sankyu to Shodan in Judo: A Long Story"
tags:
  - judo
  - shodan
  - sankyu
  - black belt
  - brown belt
  - promotion
  - history
  - competition
mp3: 'http://ju.kylebaker.io/music/full-of-zen.mp3'
cover: '/img/judo-black-belt-bow-bw.png'
date: 2017-01-22 14:38:45

---

This is a post I wrote many years ago. I'm including it here as an archival part of my history. It's pretty long, so probably not of interest to many people, but it's here for the sake of completeness.

---

I recently read [a story](https://medium.com/@Rapton/ending-an-elite-athletic-career-as-an-athlete-on-the-spectrum-cf139ecc8299#.fk30mfftt) about a semi-pro American Judoka aging out of his youth... It really resonated with me, and it has inspired me to write about my recent competition season, to hold onto the feelings I have now, in this season of my life.

Over time, as the end of my 20's sits on the horizon, I have gotten small glimpses of the shortcomings of memory. I have rediscovered writings of mine, and looked at old medals and realized how tenuous my memory of those matches becomes. In spite of how vivid the memories were in their immediate aftermath, how unforgettable they seemed at the time, just a few years later they all start to fade. After a couple years, I realized I needed to record some details. (I now write a small summary of the competition stats on the back of each medal in permanent marker.)

So I will write an update, now, to record this window in time, while it is still reasonably fresh in my mind.

# The Long Break

112 weeks ago, I got a brown belt (_sankyu_, 3rd kyu) in Judo.

(<i>How ranks work in Judo in the US</i>: there are three degrees of brown; <i>sankyu</i> is the first and lowest, but is taken pretty seriously because it is still brown. After brown, like virtually all other martial arts that have copied the belt system that comes from Judo, is black, and the first grade/degree is called '<i>shodan</i>'/1st <i>dan</i>).

<br />
Shortly thereafter, I got a gold in a local small Sambo comp, but then had to put my training mostly on hold.

{% instagram true 75% https://www.instagram.com/p/1WB5UrFYrK/ %}
{% instagram false 75% https://www.instagram.com/p/1WMTuUFYkG/ %}
{% instagram false 75% https://www.instagram.com/p/1WP6cVlYsJ/ %}
{% instagram false 75% https://www.instagram.com/p/1WQYRZlYtF/ %}

<br />
2015 became the year I slowed down on martial arts, though. I got my purple belt in BJJ shortly before my brown in Judo, after a great triple-gold-medal as a blue belt, every win by submission day. In 2015, my BJJ coach started saying I was looking at brown (in BJJ) by end of year. I was at a BJJ peak, then. I was teaching some BJJ classes, and between teaching and studying BJJ and Judo, going to virtually every class offered, I was hitting up to 11 classes a week at times.

But I was poor, and life had to change. I moved in to a spare room at my mom's house, for a set window of a few months, to set aside all else and prepare to enter a programming school. My only focus was teaching myself to code to get in to the best one in the country, which had a very difficult acceptance criteria--self teaching enough to get in would take a few months. I could no longer afford BJJ while studying, but I kept going to Judo... until I got accepted into that bootcamp. The program I was in was 6 days a week, 63 hours a week, plus whatever after hours work you could fit in. I had to pause Judo as well, as I started studying code, late summer of 2015.

{% instagram true 75% https://www.instagram.com/p/8rMieilYj-/ %}

I graduated. I started to train once a week, when I wasn't coding and job hunting. More than three months of sitting at a computer for so many hours a week, and I was out of shape. When you're in great shape, everything just feels kind of easy. Now, everything was work. Every practice was leaving me sore.

And then, after about three months, I got a job--through a recommendation from a friend in Judo, saying he'd seen how fast I had picked up Judo, and he believed I'd do equally well in programming.

It all seemed impossible to imagine, but this huge change happened in my personal life. I went from being a somewhat underachieving pedicabber who made almost nothing, to being able to open a savings account. I had to trade 8 months preparing, applying, studying, and prepping, 3 months of my life immersed in it, and then 3 months going after a job, but I had made it. I couldn't train nearly as hard as I used to with what limited time I had left outside of office hours, but that was the trade-off I had chosen.

{% instagram true 75% https://www.instagram.com/p/BFR9cYzFYmt/ %}
{% instagram true 75% https://www.instagram.com/p/BG-nRzvFYnn/ %}
{% instagram true 75% https://www.instagram.com/p/BG-kBQTlYva/ %}

# Training Again

But BJJ and Judo had been my life for the last 4 years, and BJJ had been in my life for 10 years. I wasn't about to give that up.

I slowly ramped back up to 'full time' Judo training--all 3x/wk that were available at my club, anyways. I tried my old BJJ club, but between not feeling challenged, feeling neglected, and feeling a bit hurt that my instructor wasn't willing to reduce the $25 drop in fee for me so I could train affordably on Sundays while I was studying and had no job for 3 months, I just didn't feel like it anymore. I briefly trained with a university club, and found myself, to my own surprise, still able to submit BJJ brown belts--in spite of being so drained that I sometimes felt like throwing up in class, and every class feeling like I just couldn't keep going. There was also a very impressive Brazilian black belt with amazing technique and, though rather short and small, in incredible shape, who I greatly enjoyed going with--every time I rolled, I wished I was at my peak, instead of just having to tap out to sheer exhaustion on the occasions I wasn't tapping to a submission... He had me on fitness, strength, and technique--and what beautiful technique. We rolled together most classes.

Still, my office moved further away from that club, and that instructor left shortly after I started to start his own club way south, so I looked elsewhere... none of the options I wanted had a price that made sense for the amount of times I would be able to train... once, maybe twice a week, since I was committed to Judo 3x/week, and just didn't feel like I could do any more physically than that and still be able to do my office job. Near the end of 2016, I did do some open mats at Eastside Austin Elite (now Dark Clan). I like them, I've always liked Derrick, I like that they (had) a Sambo club, I like the sparring partners... just too pricey for how little I could attend, especially if you don't commit to a year at a time.

So, Judo remained my focus. I knew I wasn't at my old competition shape, or, more importantly I thought, weight--now I was in the low 170's, a bad weight for Judo. Too heavy to cut down to the under 73kg(160.8 lb) limit comfortably, but quite light for the 81kg (under 178) category. Still, I missed competition season in the fall the year before. I figured I'd do some competitions just to knock some rust off, get some experience at the non-novice level, and see how the field felt.

# Competing Again: Becerra Challenge

With zero expectations, I started off at a small competition just outside of Dallas, and got two golds--one for a <span style="font-size: x-small;" title="category that 18-30 year old black belts compete in">Senior Elite</span>) 81kg division (where, admittedly, I had only one other competitor--a brown belt who did have a purple in BJJ, so, same ranks, technically, as myself--and we fought best of three) to face, even though my old division was stacked with difficult competitors), and one for a <i>Ne Waza</i> (groundwork) division, where I faced a few black belts and that same brown belt.

{% instagram true 75% https://www.instagram.com/p/BKL8U37Dm9T/ %}


{% instagram true 75% https://www.instagram.com/p/BKOvpjFjEye/ %}


{% instagram true 75% https://www.instagram.com/p/BKOwogADICm/ %}


{% instagram true 75% https://www.instagram.com/p/BKOxKuKjthV/ %}


{% instagram true 75% https://www.instagram.com/p/BKMnVRBDNMg/ %}


{% instagram true 75% ttps://www.instagram.com/p/BKOT_7vDdux/ %}

Honestly, though, I figured it must be a fluke.

# Competition 2: Go Shibata

Still looking for that learning experience, I tried another, just two weeks later--the old <i>Go Shibata</i>&nbsp;memorial tournament in College Station, where I had been to my first martial arts tournament as a yellow belt "way back" in the fall of 2013, when I had about half a year of Judo experience and had just gotten my blue belt in BJJ after a month back from a 7 year break.

This was a well run tournament that was known to pull a somewhat higher profile crowd. I did the brown belt division, <span title="(As a side note, having had my motorcycle stolen, I got a ride to San Antonio to buy a car the night before this competition, and so competed on little sleep, but with a lot of excitement.)">and surprised myself</span>--in a large bracket, I ended up with second place, and very nearly got first--in spite of being quite below the shape I was in when I had been competing before, and in a weight class higher. Competing up a weight class generally felt fine, but in the final, it felt that strength played a role, in the end--I had several near subs that my opponent just muscled straight out of, and gassed after all the attempts, I let him get a <i>waza-ari</i> pin while I caught my breath before refusing to lose that way and escaping. I lost on points--<i>yuko</i>&nbsp;to <i>waza-ari</i>. I had been too tired to even realize how close I was to winning during the match, sadly. I had been leading the whole time.


{% instagram true 75% https://www.instagram.com/p/BMDRfirj32U/ %}


{% instagram true 75% https://www.instagram.com/p/BMDQ_-3jr5R/ %}

On a whim, encouraged by a coach that morning, I decided to try the Senior Elite (black belt division) as well. Why not? I was already here.


Well, I ended up getting second in that as well. My brother and his girlfriend showed up to this one, and I kept telling them to expect me to lose the next match, but it just didn't happen until the final. Somehow, I just kept going.


{% instagram true 75% https://www.instagram.com/p/BL48EI0DA16/ %}


{% instagram true 75% https://www.instagram.com/p/BL_NpeNjVBo/ %}


{% instagram true 75% https://www.instagram.com/p/BL6gP4kDMW1/ %}

I was exhausted. I felt on the cusp of injury in a few spots. But elated! When I lost that senior elite match for first place, I was losing to a nationally ranked judoka who was an active international competitor, Kyle Wright--and he didn't destroy me. In fact, though I will definitely admit I felt like my opponent was better than me and would have won even if the reffing had not interrupted us to insanity, the bizarre reffing ruined the match and made it impossible to really be sure.

But that said, surprisingly, I lasted and did fine. He struggled to get the better of me, and it was a fight--he only threw me when I had received three <i>shidos</i>&nbsp;and started making knowingly bad choices just to avoid being disqualified on&nbsp;<i>shidos, </i>got a <i>yuko-</i>worthy throw (no force, landed on side), and the ref gave him an <i>ippon </i>for it<i>.</i> <span style="font-size: small;" title="(I can understand that might sound like sour grapes: To be clear, my opponent agreed with me after the match as well, and expressed his own frustration that our match had just been constantly interrupted when it should not have been, and that that throw was clearly not an <i>ippon. </i>My suspicion is that it was a combination of bad luck, slight ref bias towards the experienced semi-famous local competitor, and his excellent gripping strategy making me look bad. That's life).">(hover for explanatory note)</span>

After that, people noticed me. Other team coaches who had watched the match shook my hand. People were asking me questions. After that, my coach started talking about promoting me to <i>ikkyu</i> by December, <i>shodan</i> by next summer. Woah. I'd been assuming more like two years, now the conversation had turned to eight months.* <span style="font-size: small;" title="I had technically been a brown belt for two years, now, so it made sense if we counted a 'missed' promotion at a year back (2kyu), one in two months (1kyu), and another in six months (1dan/<i>shodan</i>), given my competition record: at this point, in Judo that was nearly perfect, getting all golds and one silver (at my first one, with 6 months experience, to a guy who immediately afterwards got a brown belt) between all of my competitions as a novice, and at this point had 2 golds and 2 silvers as a brown belt.)">(hover for explanatory note)</span>

Surreal. I had expected nothing this season. And now I was taking home medals and not finding the ceiling quite where I had thought I would.

# Competition 3: Houston Open

Well. Next was the Houston Open, the biggest 'non-points' tournament in Texas, often featuring some of the best in the state. I hadn't planned on going to this tournament, but the night before, I decided to go ahead and wake up early and make the drive.

When I got there, I was told there was no one in the 81kg division. (!?!?!). Apparently, with the Dallas invitational in just two weeks--that's the biggest competition in Texas, and of the The Three Big Ones in the US, worth points, featuring people from all over the country--many competitors were holding back and waiting for that. Again, the 73kg bracket, where I'd normally be was stacked. But I was about 9 lbs too heavy for that division. So, I could take a gold for no contest (gross!), or bump up to the 90kg (198lb) division and face people up to 25-30 lbs heavier than me.

What?! I was 173 at my heaviest! And had woken up early to make the drive, tired... whatever. I was here, might as well go all the way. And what the hell, I figured--if I was going to do that, I might as well sign up for the open weight division as well. I got them to make an exception because of the situation and inconvenience, and let me into the bracket right after the deadline.

If I had no expectations before, I definitely had none now!

Open weight was first. It was the main event, the side two mats are shut down, an announcer commentates to the audience. The prize for open weight is $500! ($150 for all senior elite divisions, $100 for novice divisions.)

I have my first match--very fast submission on a seemingly talented french judoka who wasn't expecting it.

Second match--and I'm facing a guy who is shorter, but is jacked upper body, about 190 lbs, black belt... and the announcer tells everyone: this is last year's open weight champ, who I hear from others around me beat a much bigger man, when he had about 20 lbs less of muscle--he later told me he had spent time bulking up because of a knee injury he'd been rehabbing the first half of the year.

Breathe. Remember. No expectations.

It was an intense match. Back, and forth, and back, neither of us sticking a throw on the other. He nearly gets an <i>ura nage</i> (suplex-like throw) on me, but he can't quite get me over, as I'm using my hips and body and even legs tangled with his to stop it--you can hear the audience gasp, as it was barely prevented.

Then, suddenly, after an attempted throw, we're on the ground. He's belly down, and I shoot a leg through and over his shoulder--I'm going for an _omoplata_--I power through my exhaustion, tying up all the loose ends, scooting, pulling, flattening, posturing, and...

he taps.

It was like a dream. I couldn't believe it.

After several more matches, I ended up getting second place, losing twice (true double elimination) to the same guy--once as my next match, and then a second time as a hard fought final. For the semi-final I also beat a team member who was a black belt and a former member of the Iranian national team (but exhausted from his very hard matches, and generally out of practice and shape--he has also put judo aside to build his professional life for some time now) with a submission on the ground. I don't know if I could have beat the judoka he faced (a very talented Spaniard). He got third, and I questioned internally whether he should actually be switching medals with me. What's more, a 260 lb teammate had also not entered for a silly reason, and I could have ended up losing to him, too, easily. There's always some luck in how the brackets end up.

But so it goes. Second place. In the open weight division. Holy shit. I was dumbfounded. Apparently, I was actually decent at this Judo thing. I had people warning my opponents of me before my matches, discussing and planning strategies to counter my game, which they remembered from the last competition. I had people I didn't know texting my coach (who didn't make this competition) about my performance. I had strangers telling me 'that was an awesome omoplata'. I had the coach of a competitor I faced as a green belt, two years before, recognize me and ask me questions and more or less try to recruit me.

Oh, and the 90kg division? Three guys, round robin--I got gold. Two wins, a submission and a <i>Tomoe Nage </i>for <i>ippon</i>.

$150 cash prize! I couldn't believe it! And second in open weight!

{% instagram true 75% https://www.instagram.com/p/BMexQouj5qd/ %}

(Above: one of my matches from the 90kg division. I felt like that was likely my best throw of the season. I was proud of this counter <i>tomoe-nage</i> I'd been working on, and I'd finally stuck the timing. For years this had been a strong move of mine in <i>randori</i>, but I'd never pulled it off in competition before this season. Now I was pulling it off regularly in senior elite divisions!)

{% instagram true 75% https://www.instagram.com/p/BMekzIYjdOd/ %}

<br />

# Competition 4: Dallas Invitational

All that was left was the Dallas Invitational. I'd absolutely not planned on going to this one. I'd won gold as a novice at this a couple years ago, and that was a medal I was very proud of--even in the novice category, I faced out of state competitors, at this competition. But at this point, I just had to find it--where was the breaking point? How long was I really going to be young enough to do this, looking at 27 looming a month out in December?

So, at the last minute, I decided to go. I made the drive out in my new-to-me car again, almost missed the walk-up registration window, had to beg them to let me weigh in (yup, still way under 81kg), got a hotel room, and waited it out until the next morning. My mom happened to be in Dallas for something else, and showed up late in the day, which was good, since there was a lot of waiting. She still waited a couple hours with me before my matches began.

My first match was, I found out, against the favorite to win, a student of Jimmy Pedro--Jack Hatton--who was #2 nationally ranked after Travis Stevens (the 5th ranked Judoka in the world, highest rank in the US), I heard. He had been on TV with Stevens and Kayla Harrison (most decorated US Judo Olympian, 2 golds) at talk shows, I heard. He had an amazing <i>uchi-mata</i>, I heard. (...And later confirmed.)

That first match, much like my Senior Elite final at <i>Go Shibata</i>, was very weirdly reffed, and so I felt like we didn't get a good match. I was given a lot of <i>shido</i>s--even shidos for stiff arming while I was intentionally <i>not even grabbing my opponent</i>, specifically to avoid the <i>shido</i>-happy ref claiming I was defensively gripping, and also got a threat from him for an 'attempted kneebar' that made ZERO sense (I know knee bars, and it would literally have been impossible to even try for a kneebar--I was doing an x guard sweep, but I digress). So, immediately into the loser's bracket--true double elimination, so one more loss and I'm out.

Well, no expectations. I at least gave Jack a hard time, had nearly gotten a bow and arrow choke on him (ref stood us up, but it was deeeep in, and he was totally immobilized), and I think I got a <i>yuko</i> on him, if I remember correctly. I was proud of that loss to someone of that caliber. (Again, like with Kyle, I made knowingly bad choices with three shidos on the board--but unlike with Kyle, he threw me with a beautiful, perfect<i>&nbsp;uchi-mata</i>).

That behind me, expecting to get my second loss and be out... I just started winning. Match, after match, after match. I had 4 wins up the losers bracket, before I faced the second favorite, and the guy who would go on to get second by a razor thin margin (three matches against Jack, each one determined by overtime <i>shido</i>, 2-to-1)--also ranked first in his weight class, normally a lower weight (73kg) than the one he was competing in today (like myself, sort of). He was relaxed, like I was, and I probably got a little too comfortable. As we were feeling each other out, I decided to do a basic attack to satisfy the ref and try him out. Instead, he immediately got an awesome counter against my mediocre left side <i>o-ouchi-gari</i>. Ah well.

It wasn't until later, when I looked at the bracket, that I realized I had tied for fifth. Kyle Wright, who had beaten me at <i>Go Shibata</i>, got 4th. 3rd was another nationally ranked guy who lost to Jack. The guy who got 3rd, btw, had exactly the same two loses (a loss to the guy who placed first and second each) as I did.

I tied for fifth out of seventeen, at one of the three major competitions in the US. It is among my proudest martial arts competition achievements, and probably by a significant margin my most prestigious. It's hard to imagine ever topping it--I can't shake the feeling that surely I'm just lucky.

# The Result

{% instagram true 75% https://www.instagram.com/p/BNDsMhBDt1U/ %}

<br />
In the end, at the approval of the <span title="black belts">_Yudansha_</span> of our club, and after a satisfactory display of the <i>nage-no-kata</i>... On January 22, 2017, I tested for, and received <b><i>shodan</i></b> (1 dan) in Judo, at Kokoro Judo, in Austin, TX.

Kokoro is a small non-profit club in a town without much Judo. We're also a young club. In spite of that, we've had some great talent from around the world come in at different times, and I've been greatly enriched by my time here. Glenn Macias, who runs the club, and started it while at ikkyu only about 6 years ago, is one of the humblest people I know, and I have the utmost respect for his character, and his Judo. While he likes to make jokes about the quality of his own Judo, he epitomizes the spirit of Judo more than anyone else I know. And I'm not just saying that. He shows up early and lays out a few dozen mats for us, every class. He has picked up and laid down at least many tens of thousands of mats for us--those mats are 40 lbs each. He teaches kids, sticks it out through grueling workouts, and then keeps going and teaches adults right after--and then puts himself on the line in randori against the best and the worst, the heaviest and the lightest. He comes in whether he feels like it or not. He's there when the weather is bad. He drives through traffic for every class here--he does not live close. He knows and watches the progress of every student. He is not some natural athlete, but he still pushes himself every class, and even somehow works out in between classes still. I hope to be as dedicated at his age--Glenn, you're an inspiration.

Few people know this, but I rarely paid for classes my first couple years. Glenn knew I had almost no money. I couldn't afford my own place. I may have looked cool on my motorcycles, but I drove a motorcycle because old Japanese bikes are cheap to buy, easy to work on, and cheap to insure, cheap on gas--they were the only transportation I could afford. For a couple years there, I didn't know what I was doing with my life, I couldn't find my way. I was depressed at times. He never hesitated, never asked, never pushed, never prodded. He just let me keep training.

I knew, through that time, that I had an anchor, a place I could clear my mind, focus on a single goal, be with some kind people, and keep myself from deteriorating into the abyss.

As far as I can tell, for Glenn, Kokoro has one purpose: to give.

And I have received so much.

Thank you, Glenn.

And thank you all.

Judo does not exist for us as individuals. It exists for us only in relationship to our community.

It is a great honor to train with each of you.
