---
title: "Getting a Surprise Globetrotter Brown Belt in Poland"
tags:
  - bjj
  - brown belt
  - faixa marrom
  - poland
  - zen
  - zen camp
  - promotion
  - globetrotters
mp3: 'http://ju.kylebaker.io/music/plum-blossoms.mp3'
cover: '/img/zen-camp/jay/throw-promotion-1-177.jpg'
date: 2017-10-22 14:12:13
---

In the middle of nowhere in Poland, at a little Japanese training facility in the mountains, I was surprise promoted by a group of respected black belts. It's a pretty special memory for me.

![](/img/zen-camp/above-dojo.jpg)

It was the first Zen Camp, and one of the instructors was Oli Geddes. Though I hadn't heard of him before this camp myself, my roommate raved about him. Apparently he's a minor celebrity in the European BJJ scene, has been a traveling vagabond of sorts for years now, and dedicates himself full-time to training, teaching, reffing and competing. (He would later become immortalized on the cover of the IBJJF reffing training manual.)

![](/img/oli-ibjjf.jpeg)

So, needless to say, I had hoped to roll with Oli Geddes at some point.

For better or worse, as an overdue purple belt, I had become a bit hesitant about asking black belts to roll. I always had a mild worry that I might be viewed as someone just trying to 'belt hunt' them. In an effort to not be 'that guy', I usually just found myself waiting to be asked for a roll more often than I was willing to ask.

Well, at the last open mat of the camp, with less than half an hour before the closing ceremony, I saw Oli standing on the mats and available, and figured it was now or never. I asked if he would be willing to roll.

He was.

I wish I had a video of it. It's a blur now, but I will probably never forget a few highlights that I'll keep to myself. When we stopped, the open mat was officially already past due, and the closing ceremony was fortunately running late, but about to start.

{% img /img/zen-camp/christian-bw.jpg %}

What I overheard later was that Oli ran over to Christian and told him something to the effect that I should probably be promoted. I had rolled with Christian and Kenny Pullmans at the Heidelberg camp a couple months prior, and with David George a couple times earlier in the week. Apparently between those data points, they came to a unanimous decision.

Oli ran over to me and asked me if I had someone who was my 'instructor', that was in charge of promoting me. I didn't. That's when I realized what was probably about to happen. They didn't have any brown belts available to give out, so they asked a nearby brown belt if they could borrow his belt for the ceremony, which I gave back afterwards.

The rest is better captured in the video of the moment:

<iframe allowfullscreen="" class="YOUTUBE-iframe-video" data-thumbnail-src="https://i.ytimg.com/vi/IhA2oQTm6tw/0.jpg" frameborder="0" height="266" src="https://www.youtube.com/embed/IhA2oQTm6tw?feature=player_embedded&amp;cc_load_policy=1" width="320"></iframe>

While I officially was promoted by the council of travelling black belts, it is especially meaningful to me to think that Oli was the one who may have pushed for it. In general, though, I think getting a belt at a globetrotter camp like this is a pretty cool way to get a belt.

10/10, would recommend.

<!-- picture outside in front of the mountains with black belts -->
{% img /img/zen-camp/brown-belt-promotion.jpg %}

{% img /img/zen-camp/outside-dojo-group.jpg %}

<a href="https://www.reddit.com/r/bjj/comments/747cdo/so_i_just_got_promoted_to_brown_at_the/" target="_blank">(I also answered some questions and gave some more details in this reddit thread, btw.)</a>

(pretty photos all courtesy of [Stevie Antoniou](https://www.instagram.com/stevieantoniou/), except for the cover photo--that one is from "Jay", a fellow camp attendee I think.)
