---
title: Featured BJJ Globetrotter | November 2024
tags:
  - globetrotters
  - black belt
  - travel
  - interview
mp3: 'http://ju.kylebaker.io/music/spring-river-moon-night.mp3'
cover: '/img/zen-newsletter/1group-covercrop.jpg'
date: 2024-11-1 12:15:13

---

I was featured in the BJJ Globetrotters newsletter this month, for the second time. Here's the interview as it was shown in the email:

===

![](/img/zen-newsletter/1group.jpg)
![](/img/zen-newsletter/2promote.jpg)
![](/img/zen-newsletter/3shirley.jpg)


# Featured Globetrotter: Kyle Baker

![](/img/zen-newsletter/4teach.jpg)

## Age: 
34

## Belt: 
Black (as of a few days ago!)

## Profession: 
Former bicycle-taxi, former programmer, sometimes judo/jiu jitsu instructor. These days I work with spreadsheets and strategy while I figure out what’s next–including starting @newazaclub next month!

## How many years in BJJ: 
Getting close to 19 years since I first stepped onto the mat.

## Other martial arts: 
Judo, 2nd dan

## Where do you live: 
Divide, Colorado, USA

## Where are you originally from: 
Austin, Texas, USA

## Other fun or curious information you would like to share:
- I once gave a talk in Esperanto about jiu jitsu while staying at “The Esperanto Village” (Herzberg Am Harz) in Germany
- I was once nationally ranked in Sambo in the US (Unfortunately, I promise this sounds more impressive than it is)
- I once completed a ride that included 1500 miles in 36 hours on a 1986 motorcycle (a Yamaha FJ1200 if you’re a nerd), certified by the Iron Butt Association (Yes, that exists, and yes, I have a certificate)

![](/img/zen-newsletter/5bag.jpg)

## Tell us what inspired you to travel and train? 
I moved to a foreign country when I was 18 and lived abroad for 3 years, and I’ve later lived abroad for many of my adult years in a few countries. Growing up, I never wanted to live in my home country–so traveling has always been a big part of my life.

Doing Jiu Jitsu and Judo while traveling felt very natural, and honestly is one of the ultimate travel hacks–instantly connect to locals, cut past all the caricature reality of the modern default travel experience, make some real friends.

In this era, travel is so weirdly commoditized and provided as a disney-like experience. Every travel destination is oddly self-conscious and presents itself as a pre-packaged experience, postcard highlights to repeat for every tourist. It can take some effort to escape that experience. Jiu jitsu can be a bridge to that authentic world; I rarely travel without trying to connect to a gym, and it’s often a highlight of my experience.

## Tell us about your most recent travel and your upcoming travel – where have you been and where are you going?
I just got back from Zen Camp October 2024, where I (finally) received my black belt from the Council of Traveling Black Belts, 7 years after getting my brown belt at that first Zen Camp back in 2017. I did some matsurfing at Next Level MMA in Frankfurt, under the kind hospitality of Francesco Fonte. Also, though, a coach under him (Aris) generously spent hours with me every day, sharing stories and theories with me, discussing the origins (and limitations) of ecological theory and the history of pedagogical techniques in Judo–definitely a highlight, something I didn’t really plan, just invitations and happenstance that landed in my lap through traveling in the jiu jitsu community.

On my way back, I was hosted by another globetrotter who showed me (again) that good German food actually does exist, among many other deep and beautiful truths. 

For better or worse, I accidentally left a painting and my travel guitar behind… so I guess I have to return. We’ll see what that journey brings.

Next is a short jaunt to Peru… and then, honestly, I’m still fairly new to Colorado, so experiencing the seasons here in the mountains still feels like somewhere new and beautiful every day to me.

![](/img/zen-newsletter/6scotland.jpg)

## What are the things you enjoy about traveling?
There’s something very special about traveling when it comes to creating relationships. Perhaps it’s a bit like the internet–you’re almost anonymous, you’re leaving soon, so the openness to going deep and creating a real connection feels higher. I think at this point, this might honestly be the thing that motivates me to travel more than anything else.

## Can you give us some examples of experiences you had that makes it worth traveling and training?
I mean, the easy first answer is that as someone who hasn’t had an instructor for more than 10 years, I got my brown and black belts at globetrotters camps, both in ways that were very special moments for me.

Beyond those “mountaintop” experiences, there has been a lifetime of meaningful experiences for me on the road. I’ve been invited to teach and made close friends for life. I’ve become connected to a global web of travelers and jiu-jiteros that I encounter over the years. I’ve fallen in love, experienced heartbreak, learned languages I never planned to. I’ve accidentally been to the border of Ukraine while lost in 12 hours of deep conversation with a stranger. I’ve competed, given seminars.

One stop that was supposed to be 6 weeks ended up being 2 years, with me co-leading a gym and bringing Judo back to a town that had seen the last dojo close years before. My 2nd degree black belt in Judo came through those local competitions, too, from an old 8th dan that saw me and took an interest out there–but that’s a story for another time, ask me when we meet in person.

When I got my black belt in jiu jitsu last week in Poland, I received kind private messages of recognition from instructors and training partners and students at gyms I had trained with from all over through my many years of training. I haven’t had a home gym in a long time, but I had friends and fans who had followed my journey in some way all along the way who shared their recognition and celebrated with me.

I could have just kept down the corporate path and have more money saved in the bank, trained less on the side. And… I am so glad I didn’t do that. That’s fine for many, but it just wasn’t the life some of us were made to live, and I found so much value and meaning living this way. 

## What has so far been the most surprising experience for you when traveling?
How it always just works out, one way or another. You don’t need to stress, just take everything as another story in the making.

Other than that, maybe I’m most surprised by how little surprises me. In this era, traveling is so accessible. The world has never been smaller. The travel opportunities available to us now would have been the wildest, unimaginable dream of our ancestors even a hundred years ago.

![](/img/zen-newsletter/7nc.jpg)

## Are you a budget traveler – and if so how do you plan for a cheap trip?
- If you’re an American, and you have good financial discipline, learning the credit card sign-up bonus game can make a lot of flying free, which is crazy. Feel free to reach out for a primer on the subject if you’re interested, I studied this way too deeply this year and wish I’d done it sooner.
- Use “Wise” while abroad to manage currency conversion–can save you a few percent otherwise lost to just changing your money into the local form, and that really adds up.
- Find the cheap major transport hub airports, and then use blabla car or flixbus in europe, or other cheap ground transport, to get to your final destination, and just look for gyms along the way.
- Go cheap, but not so cheap that your sleep is ruined. Good sleep is worth paying a little extra for, this goes for flights, hostels, bus rides, etc. Bring earplugs, bring an eyemask, take magnesium and melatonin as needed. Sleeping well is the cheapest way to stay healthy and enjoy everything in life more–what good is a day of travel where you’re miserable and groggy? Priorities!
- I wasted the potential of some of my early travels by being too frugal. This is the time to enjoy! Make it count!
- Carry-on-only is the way to go if you’re doing a longer trip. Get a 60l backpack (I swear by the patagonia black hole duffel) and a deceitfully spacious shoulder bag (I use a bag I’ve had for 20 years from Timbuktu) for the ‘personal item’, you can live out of them if you’re careful. Less is more, you can always buy what you need on the road. No matter how efficient I try to be, somehow it seems there are always some things at the end of a trip that I realize I never touched.

## If you were to pass on travel advice to your fellow Globetrotters, what would it be?
Travel now, not later. Old age comes faster than you realize. You can make up the money later, but youth and time are passing you by. You never know what doors will open and how your life will change.

I leave the reader with two quotes that have guided me on my way:

<i>“Doesn't everything die at last, and too soon?
Tell me, what is it you plan to do
with your one wild and precious life?”</i>

*“The secret for harvesting from existence the greatest fruitfulness and greatest enjoyment is — to live dangerously.”*

![](/img/zen-newsletter/8brown.jpg)

