---
title: "Pedagogy: One More Battlefield of Ideology"
tags:
  - teaching
  - archives
mp3: 'http://ju.kylebaker.io/music/flowing-water.mp3'
cover: '/img/teaching-knee-belly.png'
date: 2015-07-16 14:38:45
---


This is an old post from 2015, that I'm saving here as a kind of historical note on my own thoughts on teaching and how it has evolved over the years.

---

There is a never ending conflict between personality types that battles itself out in every field. The structuralists vs the deconstructionalists. That's what it comes down to.  

We see this in martial arts as a never ending dialectic. But in martial arts, as in most realms in life, the structuralists control the dialogue.  

"Drill, Drill, Drill." That's the message. It is the wrestler's credo, and BJJ culture in America has a widespread built-in respect for it, because that's our martial tradition... and because we're a structuralist society.  

But there is another voice.

"The way to win in a battle according to military science is to know the rhythms of the specific opponents, and to use rhythms that your opponents do not expect, [producing formless rhythms from rhythms of wisdom.](https://books.google.com/books?id=pytlmFWaJVEC&pg=PA21&lpg=PA21&dq=formless+rhythms&source=bl&ots=kDu-jf2Enp&sig=URQrQ2yf5kdIscARvNBFGnbGME4&hl=en&sa=X&ved=0CDwQ6AEwB2oVChMIv8L5gMXgxgIVUQ6SCh2GRQGV#v=onepage&q=formless%20rhythms&f=false)"

<span style="font-size: large;">_Creativity vs. Intensity._</span>  

Kit Dale is a deconstructuralist who rejects the dogma of structure and repetition.  

>“if you look at each situation in jiu jitsu as a something akin to a math problem or equation, then a technique is just one of several possible answers to that particular equation. Not only that, but in BJJ things are never quite the same. Something is always shifting or moving, so to apply the same answer to an equation that is forever in flux will lead to failure more often than to success.
>
>But if you learn the formula (the underlying concepts or principles), in turn you can calculate the equation, using the formula in the moment to come up with your own solution.
>
>Using this approach makes you unpredictable and relaxed in even the worst positions. Knowing you have the formula to find a solution, all you then need is the right timing. Understanding this enabled me to use different solutions for every problem and become unpredictable and innovative."

--

>"I think today’s Jiu Jitsiero are too often willing to substitute study with strength training, innovation with imitation, problem solving with repetition drilling and expression with mimicry."  

--

>"It’s easy to over complicate things, but to simplify things takes intelligence."  

--

>"The ultimate goal in any art is to articulate what is in your mind into physical form – to honestly express yourself through a creation or performance.  
>
>In relation to Brazilian Jiu Jitsu this would NOT be by rehearsing rigid forms and patterns as responses to situations. Instead, it would entail ‘in the moment’ innovations. This approach allows one to remain fluid and deal with any situations which arise in ways that are both unpredictable and intelligent.  
>
>To drill something into muscle memory to the point where you don’t need to think but just react, is to become entrapped by that technique. And to chase perfect technique is like a dog chasing its tail around in circles."  

--

While he expresses his belief that this is a revolution, he's only half right. Some people will never be won over, because at the foundation this is a clash between biologically based paradigms. He seems to come to terms with this in another blog post:  

> "If you brought me Student A and said,  
> “Kit, here is a very hard worker. They will listen to every word you say and do everything you ask. They’re strong and athletic, but can’t problem solve their way through a rotating door.”  
>
> My recommendation is for them to train 4 sessions a day. Break down each session into 70% high repetition drilling, 15% specific training, 15% sparring.  
>
> My opinion is that a focus on concepts and fundamental dynamics of jiu jitsu would only prove to confuse Student A, and could serve as a deterrent. Let’s give this student techniques to drill, the conditioning to push the pace and the mindset to implement. They COULD be the next world champion, but PROBABLY won’t ever be the next world class coach or jiu jitsu philosopher (goals aside).  
>
>I would also highly recommend Student A to focus on only a small area of the game and specialise in it. This will give them a positional edge in competition and they will be able to draw people into their area of expertise.  
>
>Attempting to learn a wide variety of techniques and positions and become well rounded will only prolong their learning progression. This path is usually an 8 -12 year black belt journey due to high volume of procedural information they will need to absorb, and eventually convert muscle memory.  
>
>Ultimately, what we would have is a jiu jitsu specialist. Someone with a very select skill set; a “one strategy fits all” approach.  Forced to impose their will upon their opponents with reckless abandon.  
>
>Conversely:  
>
>If you brought me Student B and  said,  
>
>“Kit, Student B is a highly intelligent. They have huge potential, but they lack enthusiasm. They are lazy and have no focus.”  
>
>Under these restraints I would recommend Student B spends as little time drilling and repeating as possible. Only enough to become physically aware of the technical requirements of any given technique. More focus must be exerted on learning the fundamentals and concepts; internalising information to knowledge rather than building muscle memory. From here Student B can apply this knowledge to discover their own style.  
>
>As an instructor and/or coach attempting to force a “lazy” person to work hard you run the risk of killing their motivation for training and make it unenjoyable; your student may even quit once the going gets tough.  
>
>But, what I am suggesting is that we teach them the strategies, fundamental and concepts in jiu jitsu. Then put them in an environment where they have plenty of rolling time to explore and develop. Ratios as high as 50% specific training, 50% sparring – broken up into four sections with Q and A’s to critically appraise and correct. The outcome is often an enjoyable one and it fosters an environment of growth and development without stagnation; allowing Student B to find a lifelong passion and endeavour.  
>
>This is the type of grappler will harbour the potential to take jiu jitsu to another level. Having a deep understanding and fluency in the language of jiu jitsu; being able to translate and teach it to others – conveying their knowledge back to raw information.  
>
>I feel this will also aid in extending mat time into their old age. This is because they will have developed a game that does not rely on fitness, strength and conditioning – but on technique, critical-thinking, problem solving and strategy.  
>
>I would recommend this type of grappler to study all positions aiming to become proficient and fluent in as many positions as possible.  
>
>There is no need to clutter their head and internalise thousands of techniques or drills – rather 30-100 concepts and a sound understanding of the fundamentals. This creates a faster rate of progression. The benefits are usually slow at the start, but growth comes exponentially faster.  
>
>The more fundamentals internalised, the more energy they can put into problem solving and innovating – using trial and error to come up with their own identity of jiu jitsu . Their own brand and, most importantly, their own expression of jiu jitsu.  
>
>This will usually take 4-8 years from white to black whilst creating a well rounded, creative strategist."  

<b>Here, he has grasped it; there is a continuum of personality, of paradigm, and it ranges from the form-_obsessed_ to the form-*less*.</b>

Martial arts stagnated under the direction of the form obsessed. They brought us a mirage of reality by way of what are today referred to as "TMA", traditional martial arts. Repetition, drilling, *kata*.  

Then, a new era dawned.  

Jigoro Kano was a frail man in the late 1800's in Japan. The foundation of his art was not Katas; it was real, intense sparring. Katas were there. Perfect form theoretically still existed, and to this day, in Judo, there is a difference between the 'ideal' Judo and what is used to win the Olympics, illustrating clearly the history from which the art comes.

One of his students ended up teaching the men who became the source of a new movement in the Americas, under the banner of 'Brazilian Jiu Jitsu'. Katas were lost in the transmission. The art was transmitted in only a sense--the pragmatic sense--and then rebuilt. Freed from its stale forms, innovation and invention flourished. Judo specifically was designed, as a collection of techniques, to be comprised of those that existed in the middle ground of 'effective' but also 'safe to practice at full speed in sparring without maiming your partner'. As such, it provided the perfect seeds for a revolution.  

BJJ took over the world _not_ because grappling is better than striking. It took over the world because it was deconstructuralist. Those with a mind for formlessness generally could not stand the drudgery of repitition required in striking arts, and so were filtered out. But in BJJ, they found something interesting enough to captivate them. There was no more repitition--there was just play. Creation. Expression. _Freedom_.  

But we live in a structuralist society. Structuralists are more likely to do something like start a school and show up on a consistent schedule.  

Some structuralists rise to the top by brute force. Brute force should never be underestimated.  

But it is the deconstructuralists, the free form players, that epitomize the sport. They are the ones who, being taught the techniques, see an underlying philosophy, grasp it, and expand upon it. Creation happens as a matter of course, without thought, as an expression of that philosophy.  

This is the gift of deconstructuralism. But it comes with a price: a general lack of discipline. And that is the vacuum the structuralists fill.  

And that's why the war never ends.
