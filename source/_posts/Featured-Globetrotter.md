---
title: Featured BJJ Globetrotter | September 2017
tags:
  - globetrotters
  - purple belt
  - travel
  - interview
mp3: 'http://ju.kylebaker.io/music/spring-river-moon-night.mp3'
cover: '/img/featured-traveler/day-has-28-hours.jpg'
date: 2017-09-17 12:15:13

---

I was featured in the BJJ Globetrotters newsletter this month. Here's the interview as it was shown in the email:

===

# Travelling from Texas: Kyle Baker

"You will regret the trips you didn't take, not the ones you did, so take all the chances you get!"
<b>- Kyle Baker</b>

<b>Kyle from Texas is currently travelling around Europe, hitting some Globetrotters camps on the journey! We caught up with him to see what inspired him to travel and train along the way.</b>

<b>Age:</b> 27

<b>Belt:</b> Purple

<b>Profession:</b> Programmer

<b>How many years in BJJ:</b> 11.5

<b>Other martial arts:</b> Black belt in Judo

<b>Where do you live:</b> Technically nowhere--but right before this trip, my stuff was put into a storage unit in Austin, Texas.

<b>Where are you originally from:</b> Austin, Texas

<b>Other fun or curious information you would like to share:</b> I have two Certifications from the Iron Butt Association!

<b>Tell us what inspired you to travel and train?</b>

I was introduced to BJJ when I was 15 (I think? It's been a while...) by a kind family friend who took an interest in me. He gave me my first gi. I spent all the money I had on it until I ran out, and trained sporadically as I could afford to from then on.

When I was 18, I moved to the middle east for 3 years, and trained a bit there. I would train when I visited family several hours away.
I've always known I wanted to travel, and probably live abroad. I love languages, and speak several (no comment on how well!). I started learning Esperanto in part back when I was a teen because it had a book-based service like couch surfing called "Pasporta Servo" (Passport Service) that went back to the 70's which sounded awesome, and joined Couchsurfing over ten years ago as an early adopter there as well. I don't know how I first heard about BJJ Globetrotters, but it was before the idea of 'camps' or even any merch existed, and I "joined" the website's list of members straight away, too!

So mixing BJJ and Judo, things I know I will be doing for life, with travel, which is just how I live my life, was always kind of a given.

![](/img/featured-traveler/judo-guard.jpg)

<b>Tell us about your most recent travel and your upcoming travel – where have you been and where are you going?</b>

I started in Northern Ireland, where some family just recently moved to, and visited a gym I saw mentioned in this newsletter! From there, I visited Dublin, and have stayed with a good friend in Lisbon, Portugal for most of a month, and also took surfing lessons for a week up north. Next stop is Spain for about a week, I think. Then I'll be in Germany for the Fall Camp in Heidelberg, hit a tiny place that calls itself "The Esperanto Town" in North East Germany, hopefully Matsurf a bit in Germany near the south east, have my first verified Matsurf experience yet planned for the Czech Republic, and from there I'm planning to go up to Poland for the Zen Camp!

After that, I'll return to Northern Ireland for a bit, see family and get some training in again with our dear newsletter writer and the cool folks at Kyoujin BJJ, and then probably head back to the US for a hot air balloon festival in New Mexico!

The question then is - do I move back to Austin, or keep travelling? I'm thinking central America, probably Nicaragua right now, though South America is a contender. I'd like to actually get better than "OK" at Spanish.

![](/img/featured-traveler/purple-promotion.jpg)

<b>What are the things you enjoy about travelling?</b>

Language, food, nature, culture, history. We forget the routine, the repeated. Life is short. We learn about ourselves and the world most, and live the most, when we're mixing it up and seeing more. It's almost impossible to see the things about our culture and perspective that we take for granted until we're in a place where those things are no longer 'normal' - I think we find more of our real selves when we look at who we think we are in the context of other 'normals' out there.

<b>Can you give us some examples of experiences you had that makes it worth travelling and training?</b>

-Troubleshooting an electrical breakdown on my 30 year old motorcycle on the side of a dark highway in Mexico with a kind stranger in my questionable Spanish. So satisfying to be able to end up hot wiring it and make it to my friend's place later that night.

-Flying one way to a broken motorcycle to buy it, fix it on the side of the road, and drive it 3000 miles back home over the course of a month.

-Hitching a ride with a semi trucker across half of the US.

-Living on a small farm in France for a month

-Staying with family in a tiny village in the mountains of France and having a getting to be a part of a traditional Chillean Asado and so many more!

It's been a lifetime of experiences that has truly changed everything about myself. My political and religious views are the opposite of what they were before I started travelling, and continue to grow and evolve with every encounter.

![](/img/featured-traveler/purple-naga.jpg)

<b>What has so far been the most surprising experience for you when travelling?</b>

I'm continuously surprised by how welcoming and friendly the world is, and how beautiful nature is. It's something you can count on, but it never stops humbling and impressing me.

<b>Are you a budget traveller – and if so how do you plan for a cheap trip?</b>

Major costs while travelling are transport, food, and lodging.
For transport: Bring only a comfortable carry-on-ably backpack--the farther you can comfortably walk, the more options you'll have. Even do three months of travel, I'm using a 60 liter duffel with backpack straps--technically it barely fits as carry on when I have my two travel gis in, but if I ever find an airline employee who has a problem with it, I can put on the gis and crush it down.
For lodging: Couchsurf--it's not just cheap, it's far richer experiences than the typical tourist path.
For food: cook in kitchens (I do eggs and butter and milk everywhere I go as my staples, figure out what works for you--oatmeal is another great option). Minimize eating out. I've found in portugal, I can find fresh grilled chicken in supermarkets for stupid cheap, which is a nice, relatively healthy treat. I try to minimize the bread I eat, but that's obviously cheap as well. I personally justify eating out by bringing my host out when I do--getting us both a meal occassionally is cheaper than me paying for lodging, and I eat better as well, and get to treat them.

<b>If you were to pass on travel advice to your fellow Globetrotters, what would it be?</b>

You will regret the trips you didn't take, not the ones you did, so take all the chances you get! The world has a way of just working things out. Don't stress about the details, just figure it out as you go. The less you lock yourself into, the less stressed you'll be, and the more open to adventure and unexpected opportunities you'll be.

<b>Thanks for sharing your stories with us Kyle - it sounds like you are off on a great adventure and hopefully you'll decide to keep on travelling!</b>

