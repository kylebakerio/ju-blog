---
title: "The Weight of the Belt"
tags:
  - promotion
  - blue
  - purple
  - student
mp3: 'http://ju.kylebaker.io/music/full-of-zen.mp3'
cover: '/img/nate-promotion-blue-16-9.jpg'
date: 2019-05-07 03:10:41
---

What does a blue belt _mean_? Actually, what do any of these belts mean?

The first answer to pop into your head is probably that it indicates a certain level of competence--say, a basic grap of fundamentals, a decent survival ability, etc.

That's not an inherently wrong answer, I think. It's what I said a few weeks ago, when asked.

Then, along with Bobby and Rich, we promoted our first blue belts (and our first purple belt) at Open Source Jiu-Jitsu a few days ago.

I've been here for basically half of the gym's existence at this point--nearly a year, and the gym has been open for nearly two years. None of these now-blue-belts showed up for the first class after I did... but some did show up _just_ before me here. Some I've given private lessons to. All of them I've sparred with, given advice to, taught, worked with. I know their games, their weaknesses, their strengths. For these students, I've been here for most of the development of their game--and if they are asked who their instructor is, I imagine my name comes up alongside Bobby here.

To realize that is, in itself, pretty cool, and honestly it's something that kind of snuck up on me. While I always imaged myself being a black belt one day (_still working on that_), I hadn't really thought through and realized that would mean promoting students myself (that of course seems obvious in retrospect). But you just keep doing jiu jitsu, and eventually people start asking you to teach jiu jitsu, and then a few years in you realize someone points to you and says, 'this is the guy who taught me what I know'. (If you keep doing jiu jitsu, there's a good chance that'll happen to you, too, one day.)

In the months and days before the promotion, Bobby and Rich and I discussed who was ready for blue belt. In those discussions, I felt I knew what blue belt meant. I'm on the mats 6 days a week, 17-20+ hours a week, teaching students from ages 6 to 53, from beginner to advanced, from BJJ to Judo, gi and no-gi, private lessons, etc.. I've trained and taught at dozens of places around the world, now. I know what this belt means, right? A certain level of smooth technique. An ability to handle most anyone who walks in the door untrained. A decent ability to execute and convey the fundamental skills of jiu jitsu. In other words, it means competence.

But as I spoke about students and called them up, putting new belts around them, shaking their hand and hugging them and congratulating them... I knew this was a meaningful moment for me... but up close, just far enough away from the other students to where they couldn't quite see... some of these guys were trying to stop themselves from crying in public. 

Wow. The weight of the moment caught me off guard a bit.

Sometime in the next week, as I was trying to communicate that experience to someone who doesn't do BJJ, I found myself straining to convey to an 'outsider' why this was so moving. My attempt was something like this:

In the western world, many of us live in unnaturally safe environments. Most people will probably never get into a fight in their life, and of those that do end up in a fight, it's probably not going to be serious. But deep in the heart of man is the long history of nature, where the need to be able to fight is an inescapable reality--to defend yourself, your loved ones, your honor. I've read that some primates (chimps?) hold "elections" of sorts by fighting every few years, and your status in the tribe is determined by the results of this sorting.

As a result, I think, most all of us humans walk around with some part of us, deep inside, aware of how we'd do in a fight. And it affects us. It may seem to make no sense on the surface, but when two men have a conversation, the tone of that conversation is different if one man feels like he couldn't win a fight against the other one and they both know it, than if they view each other as equal "threats". Likewise when a woman talks to a random guy on the street, the anxiety she may feel is often directly connected to the sense of threat and powerlessness that results from some part of her telling herself, deep inside, "if I have to fight, I will lose."

More than a few men have and maintain the illusion that they would win a fight, if they got into one. They tell themselves that they would just go _so hard_, put _so much effort_, that _surely_ they would win. They usually don't evaluate this thought too strongly--it's more an intuition than anything, just a feeling, a raw unevaluated belief. To think about it soberly for more than a second, of course, is to realize the asburdity of the belief--is to realize that other men also have this held-back effort, waiting to be likewise used.

It's an illusion that allows a certain lie of confidence to remain, though. It's comforting. To face that actual vulnerability, to realize that inability? For many, that's to face a question to your masculinity itself.

That's one experience, and is a reason some men never work up the courage to walk in the door and shatter that illusion (the way a part of their subconscious knows would happen, even if they choose not to acknowledge it). But this same inner reality manifests in many ways for different kinds of people.

- The men who instead feel the wound of believing they would lose a fight, and live with that wound, feeling weak and powerless.
- The women who have to walk around the world constantly feeling smaller and weaker, most of them experiencing the fear of physical inability as an inescapable and traumatizing reality.
- Those who know people around them believe that to be the biggest person in the room is to be invulnerable... but at some level feel their own lack of coordination, speed, and balance--and the resulting silent inner fear and vulnerability.

When these people walk into a BJJ gym, when they soon spar for the first time--when they are faced with their inability--they experience something. For some of them, they unmask an illusion that was a part of their identity. For others, they are bravely facing the fears they've been holding inside, and making them a reality they must stare in the face.

And you know what most of these white belts experience, for their trouble?

Loss.

After loss.

After loss.

Again, and again.

For _months_.

Some people go half a year or more, losing

every.

single.

round.

Sparring in BJJ is not like playing a board game. It is visceral. It is everything within you straining to breathe, to move, to free yourself, to find control, to escape. It is feeling trapped. It is experiencing helplessness. To lose in BJJ is not to merely face your lack of being clever, or to become aware of a deeper strategy (though it is those things, at times, too).

No, for us, the blood, sweat, and tears are real. In the rest of our society, so many of us lead bloodless lives in air-conditioned, sweat-free, touch-free, reality-free, disassociated worlds of disconnection from anything that feels meaningful.

To lose in BJJ, it is said, is to face your death. Your weakness. It is to find the end of yourself, and become acquainted with it--usually, for the very first time--and, slowly, to become familiar with it.

And in that process, we are changed. We find a part of ourselves we never knew. We experience victories that feel so much more meaningful than most of the hollow accolades our society can provide us with. And in the best case, if we're open to it, we slowly replace the illusion with humility, and on that foundation of humility we slowly cultivate genuine strength.

If you haven't done BJJ, this may sound like hyperbole. I understand, but I assure you, until you've experienced it, you just don't get it. I am telling you grown men cry when taking their white belt off, but in reality I've talked to men who have told me they cried after getting stripes on their white belt on the drive home--men, with real accolades in the world, with decades of life and accomplishments, who tell me that their stripes on their white belt are _literally the proudest accomplishments of their lives_.

A stripe, a belt--getting these, do you know what they mean? That cannot be understood without understanding this journey. The path these men and women walk is one of becoming. It is a process of growth that has few possible equivalents in the society we've created for ourselves. We, living out our artificial lives, reconnect with our neglected humanity, face the fears and lies we have built ourselves on, and grind through the slow process of growth day by day.

And then a moment comes. A moment where a teacher who has studied you, guided you, taught you, destroyed your ego, and built you up--a moment where this teacher calls you out, and tells everyone:

"Look at this one. Look at their growth. I am proud of them. I acknowledge them. I recognize them. And now everyone else must as well."

And then in that moment, all of that struggle, pain, loss and change comes to the surface, because to acknowledge their growth in skill is to call out the evolution in their humanity.

I don't think I really understood that until this day.

I can't help but take this role with a bit more weight. What an honor, to share this with my students.
