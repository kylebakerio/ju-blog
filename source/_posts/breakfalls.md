---
title: "Breakfalls: Misunderstood & Mistaught"
tags:
	- judo
	- breakfalls
	- standup
	- throws
	- takedowns
mp3: 'http://ju.kylebaker.io/music/full-of-faint-sadness.mp3'
cover: '/img/irish-tai-otoshi-scratch-2.jpg'
date: 2019-10-5 03:10:41
---

**I started off Judo pretty skeptical of breakfalls.** They looked like something very 'traditional martial arts'-y, people doing some unnecessary movement that no one could explain the theory for in a way that made much sense, but that made a lot of noise and made you look like you knew what you were doing. I came from a bit of wrestling and BJJ into my first Judo class, and hadn't ever seemed to need breakfalls before--how important could they really be?

In Judo, it is often repeated that the most valuable practical skill gained is likely the breakfall. The idea is that most of us are pretty unlikely to get in a fight and really need to use Judo for self defense, but most everyone is going to fall at some point. Thing is, falling (and, say, [breaking a hip]("https://www.webmd.com/osteoporosis/news/20000208/fate-worse-than-death-broken-hip#1")) is a [really big deal]("https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3597289/") when you're older. (If you don't want to click the links: it's a major cause of death among the elderly.)

What a shame, then, that this skill is pretty much botched throughout a large segment of the BJJ community.

People often teach (even sometimes in judo classes, sadly) that the point of breakfalls is to not post, but that's only at best half of the point. Yes, you shouldn't post reflexively, but it's also important to note that sometimes you may choose to post--to cartwheel out of a throw, or leandro-lo style active-posting, for example.

The real point of breakfalling is to learn how to take large, powerful falls safely, and not wrongly posting is only the most basic element. Big falls are still going to hurt if you only stop posting--that's why we have the instinct to post, to stop force build-up as early as possible. (It just might mean you break your wrist to save your ribs or skull or pelvis.)

The missing ingredient is slapping the mat hard **before** your torso hits. People very often instead hit _after_ their torso hits. Unfortunately, for some reason, when people try to imitate someone breakfalling, the natural first attempt is almost always is to treat your hand like the far end of a whip, and to sling it to the ground last. This does generate a very hard hit, but it does nothing to relieve the impact of the fall on your torso--it's too late, the torso has already taken the full force.

When you slap the mat hard right before your torso hits, you dissipate impact force and provide a counter-force--think of it like retro-rockets applying force in the opposite direction for a spaceship as it goes to land on mars or something, softening the impact.

When I teach on breakfalls, I demonstrate this by jumping in the air and landing straight on my back. With a proper breakfall, it's mildly unpleasant but fine. Without a proper breakfall, you'll get the wind knocked out of you.

## How did things get to this state? Why are breakfalls so often taught wrong?

When we practice breakfalls by squatting and rolling backwards and slinging our hands out, for example, there is no feedback mechanism that allows you to correct a mistake. A wrong breakfall won't feel really bad, so you don't auto-correct towards a good breakfall automatically. Problem is, if you practice breakfalls right off the bat by jumping in the air and landing flat on your back, the punishment is too severe the first few times. The soft-roll drill is designed to give you a scenario to practice the motion of a breakfall, _without creating a condition where you actually need a breakfall_. In one sense, this is great, because it's _safe_. But that safety comes at the cost of a feedback mechanism, and one therefore relies on having a competent, knowledgeable teacher who doesn't teach it incorrectly and who identifies mistakes as they happen--an external feedback mechanism.

The other things is that we're breakfalling out of a roll, and when you can roll you don't need to breakfall. That may sound like I repeated myself, but this is a different problem. You can just choose to if desired, if you want to stop the roll. But when thrown flat onto your back (a la Judo), you usually can't roll. So lots of people, practicing breakfalls, get the bright idea to just roll out of their roll instead, because it's a better answer to the thing they are drilling, seemingly. Problem is, we're not drilling how to handle 'rolling' scenarios--we're rolling to learn how to handle scenarios where we can't roll, safely. You then get generations of students, who become instructors, who teach students, that they should tap the ground as they roll on their way up to standing.

In a Judo class, you take a _lot_ of falls. You either are taught to breakfall correctly, or you learn to do it by self-correction as you're being thrown at various levels of intensity (usually softly and with sensitivity if you're new and incompetent)--that, or you quit Judo, because without breakfalls Judo _really_ hurts.

In BJJ, we so rarely do standup in most gyms, that people can go for years without having a regular enough input of data to self-correct. They then deeply re-inforce bad breakfall habits. Which makes them hate standup. Which means they avoid the only medicine that would cure the problem.

I imagine that Judo being taught to westerners in the early days was likely without nuanced and detailed English explanation. It was more "do what I do, you'll figure out the 'why' intuitively soon enough." Many of those students then went on to become the first western instructors, and then probably struggled to explain a _why_ for their practice drills to their students... Who then went on to try to 'improve' them, often losing the point in the process.

That's my theory, anyways. As for me, I never encountered a good explanation of break-falls during my Judo career. The explanations I give are reverse-engineered myself. If anyone has any corrections for me, or has any other insights into the history of breakfalls, I welcome to them.
