let resume = `
Nito Grappling Cup BJJ (Winston-Salem, NC - December 2018)
-Gold 73kg Senior Elite/Mixed (13 man bracket; $100 prize) (Won all matches by ippon)
-Gold 81kg Senior Elite/Mixed (13 man bracket; $100 prize) (Won all matches by ippon, including against nidan opponent)
-Undefeated in Open Weight (made to semifinals; due to record attendance, however, the venue closed before we could finish the division, and we were unable to finish the bracket)
-Won all 10 matches by ippon (threw ko-soto-gari, uchi-mata, & tomoe-nage for ippon; also received ippon from various collar chokes and arm attacks)
-Only one opponent scored a wazari
-Noticed by senior hachidan members of promotion committee, recommended for fast-tracked promotion to Judo 2nd dan

NAGA BJJ (Charlotte, NC - November 2018)
-Gold in 160 lb brown belt division (by submission)
-Gold in 160 lb expert no-gi division (by submission)

Grapple For Good BJJ (Asheville, NC - October 2018)
-1st place, open weight/open skill division (final vs. 215 lb purple belt)
-For charity, “light” event (one division, competitors wore costumes)


Fall Brawl NC State Championships Judo (Waynesville, NC - October 2018)
-Silver 73kg Senior Elite/Mixed (10 man bracket, double elimination, loss in final to nidan in 10.5 minute golden score match)
-Silver in Open Weight category, (4 wins, including 2 wins against yudansha -- 1 by ippon, 1 by wazari throw in Golden Score against same nidan victor from 73kg division; 16 man division; loss in final was to 240 lb opponent, who I scored wazari against before he scored ippon)
-One Open Source Judo student who attended received bronze in large 81kg senior division

Invited to stay at Open Source (Asheville, NC - September 2018)
-Asked to stay for a minimum of three months (later extended)
-Founded Open Source Judo program
-Designed BJJ kids curriculum and lead kids program
-Continued teaching theory, advanced, and fundamentals classes on rotating basis

Grappling on the Gridiron BJJ (Asheville, NC - September 2018)
-Gold in no-gi open weight, open skill division. (Featured 4 brown belts, was lightest in division.)
-Final was against 190 lb brown belt. (EBI rules, win in overtime by escape vs. no escape)

Invited to be Instructor in Residence (Asheville, NC - July 2018)
-Open Source Jiu Jitsu (opensourcebjj.net)
-Trusted to manage gym for two weeks while head instructor was out of town, teach all classes
-Invited to teach 5 part “Judo for BJJ” course

BJJ Globetrotters Camp Instructor (Portland, ME - May 2018)
-Taught “Famous Fight Analysis” - Kron Gracie vs Marcelo Garcia, ADCC 2009

Submission FC BJJ (Austin, TX - February 2018)
-Gold in Gi; faced 270+ lb opponent; won by submission
-Gold in expert no-gi; won best of 3 (two submission wins)


Go Shibata Judo (College Station, TX - October 2017)
-Gold in 81kg Senior Elite (3 matches, all opponents yudansha, all wins by ippon)
-Two ippon wins against Zach Godbold, sandan, former member of Olympic Training Center
-Win in exhibition match against -90kg yudansha by ippon

Promoted to Brown Belt in BJJ (Poland - October 2017)
-Surprise promotion
-Promoted by “Council of Traveling Black Belts”
-Push for my promotion was made by Oliver Geddes
-See more

Trained in Europe (Europe - Summer 2017)
-Trained Judo in multiple clubs in Ireland and Portugal
-Trained BJJ extensively, was promoted to brown belt in Poland by group of black belts
-Taught while visiting several clubs, in Ireland, Germany

Promoted to shodan in Judo (Austin, TX - January 2017)
-Performed all 5 section of nage no kata
-First student to go from nanakyu (white) to shodan (black) at my club
-After exceptional competition season and club teaching, promoted directly to shodan after 2 years at sankyu, a few days before the fourth anniversary of the day I began Judo.

Dallas Invitational Judo (Irving, TX - November 2016)
-D level National tournament
-Tied for 5th in 81kg Senior Elite (17 man bracket)
-Lost only to two athletes who placed 1st and 2nd (First match was with Jack Hatton, “A” rank international competitor)
-Same losses as 3rd place finisher
-4 ippon victories

Houston Open Judo (Houston, TX - November 2016)
-Gold in 90kg Senior Elite (3 man bracket, weighed about 78kg) ($150 cash prize)
-Silver in Open Weight category (winner--not me--received $500 cash prize)
-Only two losses were to same single competitor who weighed 100~ lbs more with international european competition experience
-All wins in both categories by ippon, including victory against previous year’s open weight champion

Go Shibata Judo (College Station, TX - October 2016)
-Silver in 81kg Brown Belt category (7 man bracket) (loss by single wazari points in last minute)
-Silver in 81kg Senior Elite category (6 man bracket) (loss in 81kg Senior Elite was to Arthur “Kyle” Wright, ”B” rank international competitor)
-All wins by ippon

Becerra Challenge Judo (Garland, TX - September 2016)
-Gold in 81kg Senior Elite (two man bracket, best of three, both wins by ippon throws)
-Gold in 81kg ‘newaza’ division (5 man bracket, round robin, all 4 wins by ippon)
-All wins in less than 1 minute in both divisions

Gave Paid Private Lessons in BJJ to Judo Yudansha (Round Rock, TX - Summer 2015)
-Personalized newaza lessons

Eastside Austin Elite Sambo Comp (Austin, TX - April 2015)
-Gold in Lightweight category (all wins by tko from 8 - 0 lead)

Promoted to Judo Brown Belt / Sankyu (Austin, TX - December 2014)
-Fast promotion (1 yr 10 mo) in part due to exceptional competition performance; gold in every novice division entered except for 1st comp (where I got a silver, losing to an experienced samboist)

Dallas Invitational Judo (Dallas, TX - November 2014)
-D Level Competition
-Gold in Novice 73kg

Fight 2 Win BJJ (Austin, TX - October 2014)
-Gold in no-gi Expert category (all wins by submission)

Alamo Classic Judo (San Antonio, TX - September 2014)
-Gold in Novice 73kg
-Best of 3 against a fellow Judo green belt, BJJ purple belt, all very close matches
-Competed in spite of having had a motorcycle wreck at 3am the day-of

NAGA BJJ (Austin, TX - August 2014)
-Gold in purple belt division (3 man bracket, both wins by submission)
-Gold in no-gi Expert division (3 man bracket, faced TX state wrestling champ and BJJ black belt) (wins by submission & by 12-0 point lead)

Promoted to Purple Belt in BJJ (Austin, TX - July 2014)
-Promoted by Dave Thomas at Austin Jiu Jitsu

Naturally Fit Games BJJ (Austin, TX - July 2014)
-Gold in blue belt division (2 wins by submission, including kneebar)
-Gold in no-gi beginner division (2 wins by submission)
-Gold in no-gi intermediate division (2 wins by submission, including kneebar)

Began Teaching BJJ Classes (Austin, TX - Spring 2014)
-Austin Jiu Jitsu
-Taught for 6 months

Eastside Austin Elite Sambo Comp (Austin, TX - February 2014)
-Gold in Lightweight category

Matshark BJJ (San Antonio, TX - February 2014)
-Gold in no-gi Beginner division

Houston Open Judo (Houston, TX - November 2013) 
-Gold in Novice 73kg ($100 cash prize)
-3 matches

Go Shibata Judo (College Station, TX - October 2013)
-Silver in Novice 73kg
-Only loss was to an experienced samboist who was promoted shortly thereafter

Promoted to Blue Belt in BJJ (Austin, TX - August 2013)
-Austin Jiu Jitsu / Dave Thomas

Restarted BJJ Study (Austin, TX - July 2013)
-Austin Jiu Jitsu / Dave Thomas

Began studying Judo (Austin, TX - February 2013)
-Kokoro Judo / Many Instructors

Trained at AKBAN (Jerusalem, IL - October 2010)
-They had some cross-training in BJJ and Judo
-Threw my first seoi-nage, was a beautiful moment
-Was all I could afford as a poor college student

Began studying BJJ (Austin, TX - Spring 2006)
-Vandry’s BJJ
-Studied for 6 months, then a 6 month break, then 1 more month
-Ran out of money, as teenagers do
-Participated in adult class as a scrawny 16 year old with obnoxiously flexible shoulders

High School Wrestling (Cedar Park, TX - Fall 2005)
-Cedar Park High School
`







let b2 = resume.split("\n\n")

let c2 = b2.map(entry => {
  let lines = entry.trim().split("\n")
  return {
    head: lines[0],
    bullets: lines.slice(1)
  }
})

let d2 = c2.map(entry => {

	let titleDate = entry.head.split(" (")
	let dateLocation = titleDate[1].split(" - ")
	console.log("dateLocation:", dateLocation, entry)

	return {
		
		title: titleDate[0],
		
		location: dateLocation[0],
		date: dateLocation[1].slice(0, -1),
		bullets: entry.bullets
	}
})

let e2 = d2.map(entry => {
	
	entry.bullets = entry.bullets.map(bullet => {

		return bullet.slice(1)

	})

	return entry

})

JSON.stringify(e2, null, 2);
