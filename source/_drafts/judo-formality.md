---
title: "Formality in Judo"
tags:
  - guillotine
  - sprawl
  - takedowns
  - standup
  - white belt
  - tournament
  - competition
  - beginner
mp3: 'http://ju.kylebaker.io/music/full-of-zen.mp3'
cover: '/img/oakley/comp-1-2-177-1600.jpg'
date: 2019-05-26 14:38:45
---