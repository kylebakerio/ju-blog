---
title: 'BJJ: It Really Is Judo'
tags:
---

I was at the Waynesville Fall Brawl, a Judo tournament in western North Carolina, and I saw some guys walking around with the iconic yellow Gracie Humaita patches on their gis. They had some judo instructor who cross trained and taught a bit there, I think I heard. And suddenly I felt like I had a revelation.

BJJ is really, _really_ just Judo.

It's kind of a cliche old insult in Judo circles from the early days of BJJ in the US, that BJJ stood for "Basically Just Judo". It was looked down upon as just some watered-down derivative (of which there are many, mostly calling themselves something along the lines of "Traditional Japanese Ju Jitsu", "koryu", "something ryu", etc.--).

That's not entirely fair, of course. Unlike those other derivatives, BJJ kept the culture of live sparring, which is the essential ingredient of any true martial art that hopes to maintain applicability to real world fighting.
