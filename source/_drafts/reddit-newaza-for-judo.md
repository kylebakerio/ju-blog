I wouldn't say that. I have found I can also 'throw for newaza' now, which means even 'shitty' throw attempts (a throw they can escape by diving into turtle directly) become something to capitalize on or create fear from to open up other things.

The other fact is my standing defense and balance is legitimately very good. It is extremely rare for me to get thrown. Again, with a strong newaza game to back me up, in the higher weight class when I could not win the gripping game against a favorite to win, I had the option to throw for a low percentage counter from an inferior grip, and then immediately transition into newaza.

Again, though, they either learn to correctly fear my threat from standing, and open themselves up more to my standing game, or I get to work in my wheelhouse.

Kayla Harris pretty much won every match of hers in Rio on the ground (gold in 2016). I have no shame now. And as I said, I specialize in using my lapel grip for loop choking in mid-air as they run (how I won the 81kg final in my last comp), among other things–that's highly judo-centric newaza. Also in my last comp, I turned a floating opponent mid Tomoe, who I could feel was going to escape, into a helicopter armbar–again, highly judo centric newaza. No shame in that.

I love Tomoe, but because I actually score ippon with it often. Having great newaza follow ups is just a convenient bonus. I don't have as much luck with Yoko Tomoe, weirdly. Ko Soto gari is a move I've hit for ippon, but use as a combo opener or follow up a lot. I like faking with uchi-mata and following with o-ouchi. I love and threaten moves like sode tsurirkomigoshi, their fear sets up other things (like ko-soto, or a grip improvement).

The big thing is you should still try to aggressively win the grip game when standing. That's virtually the equivalent of the positional game on the ground. It protects you from getting thrown, obviously sets up your own throws, and your smoothest follow ups onto the ground are all based on your standing grips. That's possibly the highest dividend return study in judo. You can lose (and I have) just on being outgripped by a pro.

I always want ippon every time I throw. But there is no pause as I fly into newaza.

I'm currently rebuilding out my blog, I'll have to write up a post on this. Thanks for the inspiration. Hope this helped a fellow newaza guy out.

-- https://www.reddit.com/r/judo/comments/a4njdk/i_just_did_well_at_a_tournament_and_it_has_me/ebi6mm6/?utm_source=reddit&utm_medium=web2x&context=3