Getting judo to work takes a couple years. Getting judo to work reliably in a non-judo context takes a few years more.

I consider the BJJ posture fundamentally erroneous. It's a poor imitation of wrestling posture, but wrestling posture only makes sense when there isn't a gi in the picture–all those upper body grips make leaning forward dangerous, as upper body throws become higher percentage than shots. Opposite of wrestling, so opposite stance.

Once you learn to exploit the terrible BJJ posture, you'll understand, but that's not something you'll figure out in the brginning–attacking someone who is playing extremely defensively is much harder than attacking another judoka who is engaging offensively.

-- https://www.reddit.com/r/bjj/comments/908s1g/how_effective_is_judo_in_bjj/ea5r2z1/?utm_source=reddit&utm_medium=web2x&context=3