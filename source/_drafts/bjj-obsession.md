---
title: 'Exploring the Roots of BJJ Obsession'
tags:
---

we are obsessed because things are broken in our lives, and BJJ fills some part of those holes--but instead, it should guide us to wondering about the rest of our lives, not live in BJJ as a fantasy itself.