---
title: "Jiu Jitsu Blackbelt: An 18 year journey"
tags:
  - bjj
  - black belt
  - faixa preta
  - poland
  - zen
  - zen camp
  - promotion
  - globetrotters
mp3: 'http://ju.kylebaker.io/music/plum-blossoms.mp3'
cover: '/img/zen-newsletter/2promote.jpg'
date: 2017-10-22 14:12:13
---

On October 13, I finally received my black belt. It's been about 18 years since I first stepped onto the mat.

This journey was not straight and narrow.

At 15 years old, I did wrestling for a semester; shortly after the first competition, I broke my collarbone (playing casual american football game–diving front shoulder roll gone wrong).

Healed up a few months later, a family friend who was 10 years older than me bought me a gi and started giving me rides to class. I paid $100/mo for classes, no contract, and went to the adult classes. I probably weighed about 145 lbs back then.

Somehow I had saved up $600, so that lasted me about 6 months.

I was ignored by the instructor. He rolled with me once–yanked me into a sub (I think a triangle? That was a long time ago) in about ten seconds and walked away silently. Other than that, I don't remember him ever talking to me or noticing me.

6 months later, I had $100 again, so I paid for another month. That was all the training I got until my 20s, years later.

It’s hard to remember much from those days. As far as I recall, I was mostly noted for being squirmy, bendy, and very hard to finish. I seem to recall that I spent a lot of time in bottom mount. These days, I can survive a round under just about anyone in bottom mount–it's arguably a weakness of my game that I'm too comfortable accepting that position when I'm tired.

I was never given a stripe. I didn't particularly care or give it any thought. But I always thought of myself as someone who does jiu jitsu after that point.

I moved to another country when I was 18, and lived on $400 a month, including rent and food. I biked and walked everywhere.

Outside of a handful of classes with a "Bujinkan ninjutsu" school given to me at a discounted rate, I didn't train for about 7 years. To their credit, they cross trained in everything, and had even brought in Renzo Gracie to give a seminar at some point before I was there. I did my first Judo throw in free sparring there.

But mostly, I had some lost years. I had chosen a religious path instead of university... But then I lost my religion a few years in. Moving back to the US at age 21, I had no path for a career, and no real support network. I did some construction work for $12/hr as a contract worker under the table. Then I started doing pedicabbing ("bicycle taxi"). I made enough to survive working the weekends, but I was poor, lost, and pretty alone. I had a relationship I was unhappy in but I didn't know how to survive without. My mom also lost everything she had. My grandmother got cancer and passed away after a year. My mom remarried; I felt a bit like an outsider even with her, like her new family was her focus.

—

All too soon, it was 2013. At some point I decided that even if I couldn't afford BJJ now, I could try some trial classes and then I'd know where I wanted to train later when I could afford it.

I had a grand plan to review every gym in town on a blog, but I only ended up trying classes at two gyms. At both gyms, the instructor pulled me aside and asked me about my background because I did not seem like a zero-stripe white belt, and weirdly they both said the same thing: I was clearly under promoted, and if I was there for a month I'd get a blue belt.

I felt flattered and surprised. I also was too ashamed to explain I couldn’t afford classes, so I just quietly disappeared after the trial.

My third try was the only Judo club in town.

The internet was a different place back in 2006, when I had gone to my first BJJ classes as a 16 year old–filled with far more rumors and forums and a lot fewer facts. But back then I had pieced together that BJJ had derived from Judo, and that had always left me curious about that mysterious parent art.

So, all these years later, I brought a friend and we tried it out. The warm up was brutal, run P-90X style (basically an early form of HIIT workouts that were a fad at the time). My friend never came back. Lucky for me, those warm ups were an anomaly, because I might have dropped out as well. The judo club had no single “sensei”, was low on formality, and it was a non profit run out of a Salvation Army basketball gym. Just some judo black belts with no where to train who started teaching some folks. Some of them were really good. Visitors from around the world who dropped in were encouraged to teach.

—

A few months into training Judo, I ran into an older guy at a cafe, and we recognized each other but couldn't place where from. Suddenly, he remembered: I had tried out his BJJ gym. He asked why I had never returned. When I explained I couldn't afford it, he said I should be training, and told me to come in and that we'd make it work.

I became his most dedicated student. The gym was tiny, run as a side project of his (and at a loss). I attended every class and open mat. I was also attending the 2, later 3, classes a week available of Judo.

Within a month I got my blue belt–so about 8 months of total training spread over about 7.5~ years.

Within 11 months, I went to a tournament and got 3 gold medals (blue belt gi, beginner no-gi, intermediate no-gi), with every match won by submission–and I even got some judo throws in there. I was promoted to purple belt the next day. "At this rate you'll likely be looking at brown soon", I was told at the time.

I also started competing in judo. I got silver in my first comp (loss was to a sambo guy who should not have been in the amateur division and won with an uchi-mata on everyone within less than 30 seconds). Other than that 1 silver, I got gold at every amateur division I entered, and I almost always won with ippon–either sub, throw, or pin, almost never by points.

Six weeks after getting purple, I went to a local Naga. In no-gi, as a purple belt, I was forced into advanced (highest level), in spite of my limited training history (less than two years total training, plus a bit less than a semester of wrestling). I faced a black belt that I beat 14-0 on points, then a wrestler who had competed at state, who I armbarred in less than 2 minutes. In gi, I won with a sub as well, after passing his guard and getting a quick armbar.

I started teaching classes for free as a blue belt, then purple belt, to help the gym. I remember watching Metamoris live around this time, seeing the Royler/Eddie match, the birth of EBI. One of my students from those classes got his black belt a year ago.

Between Judo and BJJ, I was in almost 12 classes per week for a while.

But I felt uncomfortable with some stuff with that coach, related to money and support, and pulled back as a result. I had no BJJ home, but I kept training Judo for a few years and doing the occasional open mat around town in BJJ. I focused on my office job, finally making real money for a while.

I achieved my brown belt in Judo, and continued to compete every chance I got; with the exception of one 3rd-of-3-bronze in my first brown belt division, I got only golds and silvers, before getting 5th (of 17) in the Dallas Invitational, a national tournament featuring international level competitors. I had an unlucky seed; my two losses were two the two competitors who got 1st and 2nd. 1st place was Jack Hatton, an olympic hopeful at the time. 

Three years and 11 months after I started judo, I received my black belt, skipping the last two ranks and going from sankyu to shodan. I was the first student at my dojo to go from white to black. As for questions on whether this is fast or slow for judo, that varies significantly per gym; what I can say is that the student promoted alongside me to black belt was a brown belt on the day I walked in to my first Judo class, so he spent more than four years going from brown to black, and I’ve always thought he was also very skilled.

—

At this point I was 27 and had been a ronin purple belt for more than 3 years. I had submitted black belts often enough that it wasn't particularly significant to me anymore, and I was more concerned with not making enemies and protecting egos than I was with belt hunting.

Around that time I was ready to quit my job, burnt out from startup life. I let my lease go, put my stuff in storage, and went on a trip to Europe. My mom had moved there, as her husband was from there, so I decided to just start there and continue on and do a 3 month backpacking trip.

Meanwhile, I had been following some folks called BJJ Globetrotters since… I’m honestly not sure since when. I can say I have a pic from October 2014 of a globetrotter keychain and sticker I had bought. Now I could finally attend a camp! Two, actually. The first Heidleberg, and the first Zen.


I spent a couple weeks with my mom, a month in Portugal training and traveling, and then attended the first Heidelberg Camp, in 2017. Fredinho was a brown belt. Thomas and Ruadhan were purple belts with me. Priit was there, before he was famous. I think there were almost 240 people on those mats. That camp changed my life in many ways that go far beyond the scope of this article. 

I was later told that I was one of the people that people were talking about. I rolled and rolled until I couldn't close my hands. I was completely dead that last day, but the experience of training with people from all over the world was thrilling. Some of the highlights were actually flow rolling with an older guy and helping someone correct a judo throw they couldn’t figure out.

I also, in the weeks after that camp, fell in love. I won't detail that relationship here, but the way that relationship wove into the years that followed in my life is inseparable from my jiu jitsu journey, and I would be remiss to not at least reference it in this history.

—

A month later was the first Zen Camp, in Poland. I bounced around Germany, training and traveling, before catching a bus to Vienna to catch a carpool to the middle of nowhere with someone else going. Apple maps caused the trip to go far longer than intended; we ended up on the border of Ukraine before realizing the mistake, and missing the first half day. Luckily, my driver was good company. I still remember fragments of that conversation to this day. (Danhi Spiller, if you’re out there reading this, reach out to me, we have some catching up to do.)

Now with my jiu jitsu sharp after a few months of intense training, I was feeling extra “guilty” for my belt, like I needed to apologize after rolls– unfortunately that was often just as awkward in its own way.

Two stories from this camp are necessary pieces of context to what happened at the end, if you’ll allow what will sound like bragging:

One of the instructors–himself a black belt under someone extremely famous–who was significantly heavier and taller than me (and fit, not obese), sought me out for a roll as "the judo guy" on the small mat, when only a handful of people were around one evening.

Respectfully, without going 100%, I threw and then armbarred him twice in a row. It was easy enough to me that it almost felt confusing.

Notably, at this point, I had a habit of not asking black belts to roll, just because I didn't want them to think I was “belt-hunting”; instead, I tended to prefer not to ask to roll with anyone, and let others choose me. 

But Oliver Geddes was there, and my roommate had been fangirling over him all week, so at the last open mat before the closing ceremony, I decided I would get over myself and ask him for a roll.

He accepted. It was a hard roll, and as far as I recall, neither of us got a sub, but at one point I got a head and arm triangle on him. I felt that if I applied myself 100% and used all of my strength, I could likely get the tap, but I had a policy of never going 100% against black belts in particular–clean or let it go. So I let it go and moved on.

We stood up shortly thereafter and he looked at me with a grin, and said, "I know what you just did."

I may have actually blushed, for all I know. I appreciated the acknowledgement as we chatted for a bit.

For those who don’t know: at a Globetrotters camp, you can request a promotion if you don't have a promoting instructor. While I had no promoting instructor, I hadn't made such a request; it just hadn't occurred to me for some reason. Somehow I didn’t imagine that applied to me.

I don't know the exact details behind the scenes, but my understanding is that Oliver Geddes went and spoke to Christian (who runs the camps), and after quickly convening the other black belts, it was decided to make an exception to the normal protocol and promote me to brown belt. Oli ran over to me right before to confirm that I didn't have any instructor back home. Because I hadn’t made a request, there was no brown belt available to give me, so they borrowed one for the ceremony that I had to return after.

It was a moment I will never forget, and it meant so much to me. What a way to end those 3 months.

Continuing with a weird quirk in the story of my life, I had two different people come up to me and essentially say the same thing right after this promotion–that I honestly could just as well be a black belt right now.

—

I was on top of the world. Freshly a Judo black belt, now a BJJ brown belt. 

Back home a week later, I suddenly got multiple offers to teach. My judo dojos talked about me leading some classes or a satellite program. A bjj gym tried to hire me after a trial class. A black belt about an hour out of town (who had started a gym because there was nothing else out there) offered to give me his gym–saying that he just wanted a place to train, but he was happy to give me all profit once operating expenses were paid if I took it over.

I didn't take any of those offers in the end. My living situation fell through, and rather than get a lease and go back to a normal job, I committed to figuring out the freelancing digital nomad thing, and my plan was to move to Ecuador with my savings and force myself to figure it out. I had a friend who said it was amazing and cheap, so I’d just sink or swim.

But as my first stop, I'd visit a little mountain town where I was invited to teach judo and BJJ for 6 weeks.

They ended up asking me to stay, and I taught there for almost 2 years full time. It was a very special window of my life; I was poor again, but very happy. I lived on less than $1k a month, in what was essentially a large closet in the gym, and later a dilapidated RV behind the gym that wasn't level… but I had everything I needed. I brought judo back to a town that had seen its last place shut down years before, and competed every chance I got at both.

I co-promoted (with the gym owner and a visiting black belt overseeing to make it kosher) my first students to blue and purple belt there. I was deeply touched to see one of my students, a grown man with a family, cry getting his blue belt.

I competed every chance I got. At brown belt, I got 9 first place finishes; outside of those finishes, at one comp I attended while injured and that I did not pull out of against my better judgment, I got two silvers and a torn rib. All of my wins were by sub, my two losses by points (and red stoppage after rib tear, I guess). It was clear already to everyone around me that I was underpromoted even at my new rank, but at brown belt, it doesn’t matter that much.

In 2019 I actually taught two very popular judo classes at the first Arizona camp. I went to a Maine camp at some point in there as well. I had an instructor suggest I maybe request my black belt at the camp, but I had decided I didn’t want it through the globetrotters at the time, that I wanted a ‘clear’ lineage, not one I had to explain to people. So it goes.

—

Shortly thereafter, I had a falling out with the gym owner. I felt deeply betrayed, and attempts to reconcile our differences failed. Given terms I found unacceptable, I left, and we've never spoken since. I still grieve that loss. Later, over beers, talking it out, a close friend and student who was a grown man cried when I explained what was happening. He said it felt like his parents getting a divorce. He’d always dreamed of me being one of the people to give him his blue belt. I keep in loose touch with a few of those folks, but I didn’t really have the chance to say a goodbye on my terms. 

I became homeless, jobless, and lost my community. At the time, I was seriously worried my mother was going to die, her health was failing. There’s more, even, that I just won’t go into in public. That was a very hard, dark time for me.

I vaguely considered moving to New Jersey to train at Gary Tonon's gym back then, as I always related to his style and shared his body type and size. But the plans didn't materialize, it's a lot to move somewhere just for a belt when you don't know anyone. I’m glad I didn’t; these days, Gary Tonon’s public persona is a bit cringe for my taste, incredible as his jiu jitsu always was.

I had been teaching at another place as a side gig. We decided to expand that program and make a gym, but... two weeks after we started the new schedule, COVID shutdowns happened.

I lived in a small travel trailer I bought with $5k a friend gave me (for which I will forever be grateful) for those months. I was deeply isolated. BJJ was suddenly looking like a poor career path. I went back to programming, building some side projects to knock the dust off, and then stumbling into my first freelance work by accident. A couple years late, but so it goes.

October 2020, I moved to Germany. I did one judo class after the first lockdowns ended, before the next round of lockdowns started again. I was invited to train underground with contacts I had, but I personally didn't morally feel ok with that choice. So I didn't train. I honestly think I trained that one judo class for all of those first two years of covid.

I continued freelancing, and lived in a moldy basement apartment. These were dark times for my mental health.

At one point I considered moving back to the US and living out of a van and training at Legion, but the job offer I needed for that failed at the last round of interviews. After roughly a year I left Germany, and after bouncing around a bit, I moved to Costa Rica. I trained for a few weeks on the coast with a kind dude on the beach (shout out Bryan @ Playa BJJ), then moved to the Central valley. Google maps had deceived me; I was never able to find decent BJJ close enough to train, but I did train judo with the teens training for the national team for a while.

—

Then my personal life, which had been rough for years, started to completely fall apart. I became a nomad again for a few months, wandering, trying to start training again, unsure what I was doing in life.

I took a break in Colorado for a month to visit my mom. I visited Costa Rica for a month and trained at what is probably the fanciest gym in the country, in the trendy neighborhood in the capital; the gym owner, on my last day, patted me on my back and told me I was ready for my black belt. I did a road trip as well in CR, taught at a small gym in the south of Costa Rica. 

Then I visited Oliver Geddes in the UK, hoping to train for a month; Sadly, I lost a week to being sick, and also lost time when a major business opportunity come up–before fizzling into nothing. I probably only got in 5-7 classes there in total in that time, which was frustrating.

Year before, I had told Oli that he had permission to promote me to black belt if ever he saw fit. That was pre-covid. I hadn’t seen him in 5 years. So I figured it was possible, though not probable, I could be promoted that month.

But it didn't happen. He never said why, and so in keeping with BJJ belt etiquette, I didn't ask. He also made it clear I shouldn’t expect to be promoted in the near future. While I hold nothing against Oli and have always thought well of him and felt he was a great host and a solid guy, and I’m forever grateful for the brown belt he advocated for… the vibe became soured. I don’t know why I wasn’t recognized by him, but the idea of being promoted by him after so many years started to feel sour in my mouth. (There’s even more to this story that is relevant–two other trips to London, before covid–but this is already quite long, so I’ll save that for the book.)

(That’s 90% a joke, to be clear.)

BJJ had become a bit depressing to me. In my late 20s, right before Covid, I was in my prime, super sharp, easily better than the vast majority of black belts I rolled with, co-running a thriving, cool gym. Now? I still looked relatively fit on the outside, and I was still an even roll at black belt level and a step above all but the best brown belts I encountered… but I wasn’t at my peak anymore. I was only getting older. I knew I had deserved this belt years before, and was somewhat resentful that it hadn’t happened at my peak. 

It takes years to develop these relationships. I had watched former students and training partners multiple belts behind me pass and reach black belt before me… and here I was, stuck. There had been opportunities, I could have probably gotten my black belt, but… I wanted it to mean something. I didn’t want to just get it randomly from someone that didn’t mean anything to me.

I had watched many former students and training partners reach black belt on social media over the years. Sometimes it felt depressing. Sometimes it felt unfair, frustrating. I had had black belts mention indirectly over the years they could get me promoted, but I didn't just want the black belt to have it. I wanted it in a way that would mean something to me.

—


Deciding that since I was on this side of the world and not getting younger–yolo–I decided to spend the money on a last minute second hand ticket to Zen camp.

It had been 6 years since that last Zen camp, 6 years at brown belt. My belt had long since frayed and faded; people were sometimes unsure what color it was supposed to be.

So I showed up at zen camp, 2023. I wasn't at my peak, but I was still decent, and my peak had been pretty high at one point. And on the last day of camp, Francesco Fonte sat with me at lunch, and completely caught me off guard when he said, "you should request your black belt".

I sat there silent, probably for a minute, while he prodded me repeatedly. I thought about it. Originally, years ago, I had decided I didn't want a Globetrotters black belt. I didn't want to have to explain my lineage to people when asked. I wanted something special.

But as I sat there, being goaded by Francesco, I realized... When I thought of getting promoted by some black belt at some gym, surrounded by strangers, it felt... Empty. Zen camp was a special place to me. All those years ago, I not only was promoted, I also felt for the first time that I had clarity on what I wanted in life. And somehow, here I wasn't an outsider in a room full of strangers; here, we were all travelers together, with several of them knowing me from over the years, and with fast friendships formed through surprisingly deep chats around campfires and hanging out in the sauna after class, over communal meals, sitting on benches looking out into the horizon.

Eventually Francesco changed his question: "do you want me to ask for you?"

After a long silence, I finally said "if you think that's what's right, ok."

Was this about to be a repeat of my brown belt promotion?

No. I was not promoted. Again, I don't know the full details behind the scenes, and there are other factors that may have been at play, but my understanding is that there were at least two primary objections:

1. Black belt is too important to give away spontaneously. All Globetrotters promotions are rare, but a black belt promotion especially so. Because of the last minute nature, not all black belts had rolled with me and gotten a chance to form an opinion.
2. I hadn't been to a camp in 4 years. Promotions are generally also given only to those who have been around at enough camps to have established a relationship and reputation.

Both of which are valid, and what you'd hope for from an organization like this doing something like this.

I wasn't really bothered. I hadn't had expectations and had long accepted my brown belt with pride. Francesco apologized to me, saying that as a 1st degree, he could not promote me himself, but he would otherwise. While that meant a lot to me, I didn't know him well enough to want that, that way.

Will Dorman came up to me later that night, and asked how I was doing. He said that he didn't really promote people, but that I was ready, and that if I wasn't promoted by the next time he saw me, he'd promote me.

As an aside, this story, long as it is, is abbreviated. When I got my brown belt, I had two other people say the same day that I could just as well be a black belt. Rich Saab pushed me to get my black belt years ago when I was teaching before COVID. In Costa Rica, I heard multiple times that I was ready. I had students at times tell me I was the best black belt they'd ever rolled with, call me a walking BJJ encyclopedia, etc. I've probably forgotten about others. It's been many years.

After that, my life truly crashed down around me. I experienced some very severe personal trauma. I became unable to work. I couldn't sleep. I trained a couple times in Cyprus, but ended up being too afraid to train in my mental state and with my lack of sleep.

I got a severe case of COVID. I've always been fairly lean, but I lost about 7% of my body weight, going down to my high school weight for the first time in 15 years.

I was growing as a person, but retraumatized, stuck in a loop. Having traveled for so long, I had a very thin support system. I've always had a fairly distant family. My mom had no home I could come to herself. But I moved near her, unsure what else to do.

I started going to open mats a bit, climbing again. Hurt my shoulder somehow climbing. Slowly got back to the low side of a health weight for me.

A gym I visited a few times for open mats lost their instructor, so I picked up some classes there.

I went to Zen camp again in the spring. It was a special experience because of the relationships I formed, but I did not pursue a promotion. I was still recovering physically. I was emotionally still a wreck.

My teaching picked up until I was eventually teaching 6 days a week. In September, some Brazilian heavyweights who had just gotten back from worlds (either one or both podium), two of them mid 40's on steroids, who were both high degree black belts, visited for a seminar, and chased me for rolls. I was worried about injuries; I had lightly torn my second rib a few months before while sparring, and had only recently started really feeling ok, neither particularly fit nor unfit.

None of the three submitted me, while I submitted two of them.

They were quite surprised, asking why I didn't compete. The two on steroids both asked for second rolls. I survived 5 rounds and got 4 subs, while none of them subbed me.

At this point I finally realized I was back to my old self.

I honestly was undecided if I wanted the promotion at this camp, this year. After rolling with them, I wished I had tried master worlds at brown belt at least once. In 20q8 I hadn't considered it. In 2019 I had a fresh rib tear. Then COVID, and then... Here we were.

I never did an ibjjf tournament – would love to see them get CJI'd one day. But another year? I was leaning towards not asking, just going and see what would happen. If I got promoted, so be it.

I desperately wanted to be at my best, but unfortunately, I slept terribly the month before camp. And as I flew to Zen camp a month later, I was informed during texts mid-flight that the gym would be closing.

At the last minute, as I was asking for rides with anyone who might be carpooling from Germany, and arrived a few days early to get over the Jetlag before camp.

Francesco Fonte reached out to me: he couldn't give a ride to camp, but wanted to offer to let me crash at his gym while I was there.

So I did. I didn't sleep on the flight, got to his gym in the morning while open mat was going on, and went to change and started rolling. After some rolls with his students and him, he pulled me aside. Now a second degree, he asked: do I want to be promoted?

Francesco told me that he had been in a similar place, stuck at brown belt. Those who had offered him black all offered it with strings attached. He refused them, and so was stuck at his rank.

And it was Octavio Couto, a real OG, who gave him his belt, saying he expected nothing in return.

And that he wanted to honor his memory by making sure I knew that offer was open to me, as well.

He was too busy for a prolonged discussion right now, so we agreed to discuss it over camp.

Frankfurt is an emotional place for me. I have some bittersweet memories there, and could easily have gotten lost in some dark places in my mins. But I found a close friend, unexpectedly, with the coach Francesco had hired. He listened as I shared my story, showed me around, and we had long discussions on teaching, jiu jitsu, and touched on some philosophy.

I was impressed this was who Francesco had hired. This wasn't just some blue belt. He found someone who had a master's in sports psychology. We talked about "Ecological method"; he knew it from his academic studies before he ever heard there were people trying to apply this to jiu jitsu. We talked about how judo is taught vs. jiu jitsu, and he listened intently, at night researching things we talked about and then coming back with more to discuss.

Within a few days, he had lent me a book and I had recovered from Jetlag, and found myself looking forward to our next meeting.

Over the summer, a friend who has been a Globetrotters instructor many times told me on a call that Globetrotters black belts are rare in part because unanimous approval is very hard to get. He said that in general, it's more typical that after an objection, one of the black belts takes it upon themselves to do the promotion.

Knowing this, I talked with Francesco, and explained why a belt with no lineage meant something to me, explained more of my history. With his blessing, I formally requested to be evaluated for black belt.

I was told that asking at the last minute like this wasn't ideal and may hurt my odds, but to send an email with my background and reasons for requesting and he'd forward it and leave it in their hands. Christian refuses himself from the decision.

I decided the next day to roll with every person at the camp, an achievement. It seemed like a good way to celebrate the moment, if it was going to happen.

I thought I may have offended one of the black belts that first day, and assumed the unanimous approval was likely a lost cause. I did not feel like I was being particularly interviewed or sought out. I wondered if the "no" decision was a foregone conclusion. I sat with my emotions and accepted this outcome. I knew I was ready for black belt; if someone did not give their approval, I had genuinely reached a place of not needing it. I wanted to seek the approval of those who recognize my value and see me. And if it was going to be Francesco choosing to give the belt instead, then it would not be a second choice for me; it would, if that happened, be what I myself preferred.

I did roll with everyone who would accept a roll, which was 82 people in the end. Three were injured, and three people were not willing to roll for their own reasons (as is always everyone's right, and a right now one should ever be afraid to exercise).

And on the final day, Francesco walked up, and said that it was unanimously agreed that this promotion was long overdue, and gave me the honor of tying the belt around my waist.

I am a man that feels deeply and cries easily. I did not cry in this moment. It was a beautiful moment, and I am deeply satisfied that I dreamed of this promotion and achieved it. I was honored to be called out in this way by Francesco, to be recognized and seen.

I wish I would have felt deeply enough in that moment to have it be a time when I cried, but a part of my journey was genuinely letting go of the belts. In all honesty, I probably should be a 2nd degree black belt at this point, if not even 3rd or close to it. I had to give myself my own acceptance long ago. I was at peace as a brown belt, and with honor took a position of humility wearing it.

The comment I of course got over and over, in person and on social media, was "long overdue". It was. I fell between the cracks 10 years ago in jiu jitsu. I only received guided bjj instruction for less than 2 years in total. My jiu jitsu has long been my own. When I roll with people, they constantly remark on how unique my style is, how they have never seen moves I do before. Two people on this trip gave me a deep compliment when they said that I had established the bar in their mind for what a 75kg black belt could be, that I had inspired them when they had watched me roll with heavier training partners.

One time I was told they heard someone else make a comment along the lines of, "if he's that good, can you imagine what world class opponents are like to roll with?"

I'm nearly 35, so my glory days are fading, so I hold onto kind words like this dearly. I'll remember them for the rest of my life.

How special, to get to be an inspiration like that for others.

It's been a long story, and there's so many things I didn't share. I had 100 competition matches between sambo, wrestling, BJJ, and judo. I've taught and trained at well over 50 mat spaces over so many years.

Keep in mind to not rush to the end. You have the rest of your life to be a black belt. Relish the journey. My brown belt, worn and faded, with me through so much pain, will hang as a memento more important than any of my gold medals or naga belts.

And if one thing sticks with you from this story, I would hope it would be this realization: that the value of jiu jitsu is found in the relationships it creates. Invest in those, don't lose track of it all. That's where a meaningful life is found.

When I think of the black belt, and my place in the community, it is about appreciating the chance to be a mentor and a guide to others. I've always loved teaching. This symbol doesn't change anything about my abilities as a teacher or competitor, but (for better or worse), reinforces my role and supports my opportunity to give back to others.

See you on the mats.
