how many gyms have you trained at? Culturally it varies from gym to gym--some schools are all about strictly plain white gis, but those are widely considered very restrictive here and many look at that negatively. Most BJJ schools in the US would be perfectly fine with lots of patches, though most people don't do many. Even though I'm a fan of the patching culture in BJJ, I have never put a patch on any of my gis, and prefer to buy gis with minimal branding.

I do like putting patches on my backpack, though. And for what it's worth, I also hold rank in Judo, which of course (as you mention) is a more strict gi tradition.

I would be curious to hear more about the patching culture in brazil for bjj gis, but in bjj, I've always found overly-formal schools irritating--it always felt like an artificial imitation of japanese culture. In Judo, it feels a bit more authentic (though, still, my judo school was relatively very relaxed--bjj gis were fine for people dropping in to cross train, odd color gis were fine, showing up late was fine. just bow in when you walk on the mats and line up and bow properly out. I didn't even get a proper judo gi until I got my brown belt.)

--

That little gi policy is a small part of one of the reasons I refuse to train at Gracie Barra or Gracie Humaita; they seem to be arbitrarily strict in a way that violates the casual, no-nonsense tradition of BJJ.

I'll drop in, but I have no interest in being a regular at those places.

Incidentally, in my experience, they also tend to be closed off, not be into open-mat culture, have sales-focused policies (contracts, not posting prices, must buy the gis they sell), being behind on their curriculum, have overly static curriculum, generally (not always!) have watered down grading, not allowing students to cross-train, etc. It's a conservative culture thing. You're into the weird and cutting edge and free thinking–or you're into tradition, history, hard lines, consistency, and care more about lineage and artifices of respect. I'm the former.

(This is an opinion that cannot be popular with a lot of people, but I mean it without disrespect–I'm just a certain type of person, and those schools don't appeal to the type of person I am. For what it's worth, I've trained in over 40 locations across the US and Europe, including visiting a couple schools of these lineages.)

-- https://www.reddit.com/r/bjj/comments/9gw64n/got_me_a_new_patch_so_please_stop_telling_me_i/e69w88c/?utm_source=reddit&utm_medium=web2x&context=3