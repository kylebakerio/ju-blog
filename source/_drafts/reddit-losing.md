Interesting, two comments saying you should see a mental health professional. I recoil at their comments, but it's because I relate deeply to what you've said.

My response wasn't to be afraid of playing, but instead I played constantly. But it was overwhelming, emotionally. Losing was very painful.

It was very helpful in life, though. I first found Go 12 years ago, at age 16. The absurd emotional rollercoaster it gave me was a revelation that I had issues. Through BJJ and Judo, and the study of languages, and in my professional life as a programmer, I've explored these same themes over and over again.

As a smart kid, everyone around me did me a great disservice by fawning over how natural I was at everything. This is because as soon as anything required effort, it appeared to be evidence of the limitations of my natural ability, the source of my value.

But no matter how good or smart you are, you have to work hard to reach the highest levels. This is true of ever master.

And, as I love to tell people in BJJ and Judo (as I now have teaching ranks in them both), the key to improvement is largely "embracing the suck" and just putting in the time.

It is exactly as you say, that losing is the key to improving. But this is a fact not of Go, but of how the algorithm of learning works for us at a biological level, for learning any reasonably complex thing. The algorithm of the brain, of our neural net, requires a large amount of input data, of success and failure, and it uses emotional responses as success/fail measurement, and slowly reprograms itself accordingly.

You'll have to reprogram your response to failure, too, to find peace. You will heave to learn to love failure. To pause when you feel the pain coming, to relax, to laugh, to enjoy the moment, to realize that in the larger picture of life, this is one of the beautiful moments on your journey. That you must lose. Be grateful for the loss, love the loss.

I'm still on this journey, too, but I've come a long way, and I'm so much happier now that I have.

-- https://www.reddit.com/r/baduk/comments/82d3tn/i_love_go_buti_think_im_afraid_to_play_it/dv9yvph/?utm_source=reddit&utm_medium=web2x&context=3