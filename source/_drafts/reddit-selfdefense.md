As for the last question, MMA has its own ruleset and context dependent variables. It's also not a pure laisez-faire test setting; there aren't open brackets, it's all invite only. How many high level Judoka have had the chance to participate vs. how many BJJ + wrestling guys have gotten to participate?

I hear all of your complaints, and will acknowledge they're valid--but I don't think the ultimate severity of those restrictions is as bad as you say. We still produce excellent, skilled athletes who could apply their skillsets in self-defense scenarios. The short guy may not go for the optimal leg attacks, granted, but he will still have options, and facing a relatively untrained attacker, he will do fine. And if he does grab the legs, as might happen in a scuffle, his fundamentals will guide him in improvising anyways, honestly.

I agree that Olympic Judo isn't optimized for self defense, but it's still way overboard to say that it's "an absolute joke".

I think you agree with him and we're really arguing about a technicality of speech here. I think the real problem is Judo clubs choosing to focus on competition instead of self-defense, if you want to call it a problem. In the end, people pick what they want, and more people will compete than defend themselves. They practice a game, and they get self-defense skills as a secondary side effect. I think that's ok. It keeps people motivated, and it's fun, and most of us won't need to defend ourselves anyways. If you want pure self defense, you'll have to look a little harder to find a club that focuses on those aspects, but they're out there too--they're just not in as high of demand.

Judo adapts reasonably to no-gi situations as well, by the way, and even a t-shirt (that you don't mind tearing) goes a long way to enabling most of what you need a jacket for in Judo.

At the end of the day, your average joe will go over with a no-gi seoi and split his head on the concrete if that's what you want, and he will never see it coming. Seriously, Judo is plenty adequate for self defense. It'll also keep you on your feet, teach you to clench (minimizing their striking potential), and keep them off of theirs.

-- https://www.reddit.com/r/judo/comments/7rcqxm/new_amended_rulesthoughts/