---
title: "Open Source Jiu Jitsu"
tags:
  - dojo
  - gym
  - travel
  - nc
  - asheville
  - open source
  - bjj
  - judo
  - charity
mp3: ''
cover: ''
date: 2019-05-26 03:10:41
---

I was on the BJJ Globetrotters Facebook page, when I saw a post that caught my eye. It was a BJJ gym seeking out guest instructors to come visit short-term, in exchange for lodging and food. They had [a link to a web page on their site](https://www.opensourcebjj.net/bjjworkexchange/) detailing out what they wanted, who they were, and what the terms were. They also mentioned that they especially would love to have Judo or wrestling cross-training.

The place looked pretty cool--a graffiti art mural on the front, some type of campground in a little nature area in the back. The mats looked awesome, huge and raised up on tires. The website looked kind of cool, industrial. The co-owner & head instructor, Bobby was mentioned in the original Globetrotters book, which was kind of neat, and was actually a really nice little blurb. I also liked the way he wrote.

The name also caught my eye, as a programmer.

<!-- insert blurb -->

I told them I'd like to come. They mentioned a week. I said I was thinking more like a month. They said they could be down with that.

A couple months later, I drove 1700 miles halfway across the country and ended up at the little gem in the mountains. The plan was to see how things worked for a couple of weeks, and then cover classes for Bobby for a few weeks while he went to Maldova to be there for the promotion of one of his early students to black belt.

He came back, and then I taught a few more classes, it had been 5 weeks. We sat down, and they asked me if I'd be willing to stay longer term, start a Judo program and help them add some more classes to the schedule. We agreed on 3 months, but it's been just about a year now.

Open Source seems pretty weird at first glance, but it's actually even a weirder gym than it seems on the surface.
