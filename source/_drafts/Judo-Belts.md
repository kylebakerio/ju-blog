---
title: "A Rational, BJJ-flavored, Historically Inspired Approach to Belts in Judo"
tags:
  - belts
  - judo
  - rank
  - promotion
mp3: ''
cover: ''
date: 2019-05-26 03:10:41
---

In the 1600's, Dosaku Honinbo, a famous player of the ancient board game _go_ designed the _kyu/dan_ ranking system. At the bottom of the scale was 30 _kyu_. This number would go down steadily until you reached 1 _kyu_. The next rank would be 1 _dan_ ("shodan"), and after that the ranks would go up until 9 (sometimes 8, sometimes 10) _dan_. The difference between these ranks would be the number of handicap stones needed by one player to have an equal chance of winning the game.

The use of this ranking system spread in Japan, and is used today in flower arranging, caligraphy, tea ceremony, and other games. In the present day, I know for example that it is also used in Japan for climbing, to rate route difficulty. In the 1800's, at least one other field to have adopted this system was competitive swimming. Supposedly, _dan_ rank swimmers would wear a black ribbon around their waist.

There was no such system in martial arts. The only concept of rank was whether you were given the 'scrolls to the school', the _menkyo_, authorizing you to teach, or _menkyo kaiden_ to eventually succeed the head of the school as the new head.

Then, in the late 1800's, came Jigoro Kano.

Kano was creating a new kind of martial art. I think few really appreciate the visionary Kano was (I'll refrain from going to deep into that topic for now, and save it for another post), but one innovation that spread throughout the world was his idea of a training uniform (a _judogi_, or, more generally, a _gi_), and a belt (_obi_) that conveyed rank. The idea of the 'gi' spread broadly itself, also being adopted by karate, and beyond to korean martial arts, and many others. The idea of the belt spread even further, though. Even arts without a gi (I've heard of some schools of capoeira, kung-fu, krav-maga, 10th planet, and even MMA) find themselves unable to resist the pull and power of a belt system of sorts (whether it be colored cords, sashes, rashguards, etc.).

In its original incarnation, in Judo, this system was dichromatic--white for beginners, and black for senior students.

Fast forward a bit. Kano sent emmissiaries throughout the world to spread Judo far and wide.

One student was [name]. He began to experiment with more belts, as a tool to increase motivation in his western students.

Another was Mitsuyo Maeda. Somehow some of his teachings reached the Gracies, who seem to have learned a minimalist, ground-focused subset of Judo. What's interesting is that Judo was in a state of constant innovation. Things kept evolving. Maeda, though, left Japan at a relatively early state in Judo's history. Based on dates we have, it seems Maeda may have never learned the triangle choke, for example, which was developed in Japan by Judoka around [date] and so not taught that to the Gracies, who seem to have not integrated that move until Rolls (the original cross-training BJJ-based grappler) rediscovered it in the 80's while studying a Judo book.

Helio claimed he'd never heard of Judo until the 50's.

It also seems that they got wind of the early version and idea of the belt system, but then operated within a bubble. They had a three-belt system; white, light-blue, dark-blue. Dark-blue was only available to those who had been invited to, had taken, and had passed the instructor's course. This is reminiscent of the early belt system, and one can imagine the variation being justified by blue being an attractive color and by wanting a difference between your thing and the thing you are deriving your art from.

But knowledge spread. The idea of white -> various colors -> brown -> black as "the" belt progression became ubitquitious. The weight and power of the idea of a _black_ belt, and of a grandmaster having a _red_ belt, and of some hybrid belt on the way for very high ranked black belts--this all now had universal currency, and anyone doing their own in-house thing with belts was missing out on the weight of saying those words that would impress even those who knew nothing about brazilian jiu jitsu: "I have a black belt in _x_", or, "I'm a _y_ degree back belt in _x_".

So the belts were overhauled and modernized by a new comittee in [year]. BJJ got a somewhat standard belt system. But, in a fashion that suits BJJ culture, the belts meant something a little different. A BJJ black belt would take significantly longer than any other martial art's black belt... because that's how it was going to be. (I'm actually curious whether the black belt in BJJ has become easier or more difficult to attain over the years. I could see an argument for both, but I just need data that I don't have and is hard to really find without interviewing lots of old-school people directly.)

So what about the present day?

As someone steeped deeply in both arts, I have mixed feelings. I find the black belt rank in BJJ excessively inflated. A competitive purple belt will tap often tap a casual black belt. What this tells me is that the "black belt rank" should probably be closer to where we put purple belt in BJJ.

This is essentially the case in Judo, where I generally think a first degree black belt is (when done right) about equal to a purple belt, and second degree about equal to brown belt, and third about equal to a BJJ black belt. Likewise, in Japan, a black belt doesn't have the same prestige that it does outside the country of origin. In Judo, you are typically not referred to as _sensei_ until you have reached 3rd degree black belt.

Purple belt in BJJ / first degree black belt in Judo both, in my mind, mark the level at which a student has high competence in some areas, broad competence in most areas, understanding of the structure of the game, and the ability to direct their own growth for the most part.

So, in this regard, I'm a fan of the Judo rank system.

-Judo: white/[brown]/black in japan, colored belts in the US
-bjj: blue -> brown
-stripes

interpretation and pressure
