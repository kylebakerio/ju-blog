things to note:

all images:
	keep them in the theme 'source' folder, not the main 'source' folder--they'll be destroyed when you run 'clean' otherwise, for some reason. :/

post cover images:
	must be in a ratio close to 16:9 (e.g. 1920/1080) otherwise they'll be smooshed. :/

that main cover polygon:
	see div id="vibrant" and diaspora.js

timeline stuff:
	has been modified a fair amount to suite my purposes. stuff is in themes -> diaspora -> source -> (timeline.css & timeline.js)
	currently super buggy on iphones, bleh.

sliding menu pages:
	see format of source/timeline/index.md & notes in diaspora/layout/page.ejs
	short version: slideIn:true & slideLayout:[ejs file path] in the YML of index
	
	info: if I use the proper hexo-defined functionality of defining a 'layout' property in the source/[folder_as_page_name]/index.md YML header, the slide-out functionality of the theme breaks. That behavior is in Diaspora.js, and is quite hacky as defined, so I have defined my own improved version.

see the todo.txt, and examples of sliding pages, and ejs templates, for guidance
