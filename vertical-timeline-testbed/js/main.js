(function(){
    // Vertical Timeline - by CodyHouse.co
    // NOTE: this is here as an example to look at for it's original design, this is not used in this hexo project-- see themes/diaspora/source/js/timeline.js
	function VerticalTimeline( element ) {
		this.element = element;
		this.blocks = this.element.getElementsByClassName("js-cd-block");
		this.images = this.element.getElementsByClassName("js-cd-img");
		this.contents = this.element.getElementsByClassName("js-cd-content");
		this.offset = 0.8;
		this.hideBlocks();
	};

	VerticalTimeline.prototype.hideBlocks = function() {
		//hide timeline blocks which are outside the viewport
		if ( !"classList" in document.documentElement ) {
			console.log("no classList")
			return;
		}
		var self = this;

		console.log("hmm", document.getElementsByClassName("js-cd-timeline")[0].offsetHeight*self.offset, window.innerHeight*self.offset)
		console.log("classList found", this.blocks.length)
		window.theBlocks = this.blocks
		window.theOffset = this.offset

		for( var i = 0; i < this.blocks.length; i++ ) {
			(function(i){
				if( self.blocks[i].getBoundingClientRect().top > document.getElementsByClassName("js-cd-timeline")[0].offsetHeight*self.offset ) {
					self.images[i].classList.add("cd-is-hidden"); 
					self.contents[i].classList.add("cd-is-hidden"); 
				}
			})(i);
		}
	};

	VerticalTimeline.prototype.showBlocks = function() {
		console.log("showBlocks()")
		if ( ! "classList" in document.documentElement ) {
			return;
		}
		var self = this;
		for( var i = 0; i < this.blocks.length; i++ ) {
			(function(i){
				if( self.contents[i].classList.contains("cd-is-hidden") && self.blocks[i].getBoundingClientRect().top <= window.innerHeight*self.offset ) {
					// add bounce-in animation
					self.images[i].classList.add("cd-timeline__img--bounce-in");
					self.contents[i].classList.add("cd-timeline__content--bounce-in");
					self.images[i].classList.remove("cd-is-hidden");
					self.contents[i].classList.remove("cd-is-hidden");
				}
			})(i);
		}
	};

	var verticalTimelines = document.getElementsByClassName("js-cd-timeline"),
		verticalTimelinesArray = [],
		scrolling = false;

	if( verticalTimelines.length > 0 ) {
		for( var i = 0; i < verticalTimelines.length; i++ ) {
			(function(i){
				verticalTimelinesArray.push(new VerticalTimeline(verticalTimelines[i]));
			})(i);
		}

		//show timeline blocks on scrolling
		console.log("adding listener")
		verticalTimelines[0].addEventListener("scroll", function(event) {
			console.log("scroll")
			if( !scrolling ) {
				scrolling = true;
				(!window.requestAnimationFrame) ? setTimeout(checkTimelineScroll, 250) : window.requestAnimationFrame(checkTimelineScroll);
			}
		});
	}

	function checkTimelineScroll() {
		console.log("check scroll")
		verticalTimelinesArray.forEach(function(timeline){
			timeline.showBlocks();
		});
		scrolling = false;
	};
})();
