Vertical Timeline
=========

NOTE: this is left here as an experiment space, but code in this folder is not used in hexo.

An easy to customize, responsive timeline.

[Article on CodyHouse](http://codyhouse.co/gem/vertical-timeline/)

[Demo](http://codyhouse.co/demo/vertical-timeline/index.html)
 
[Terms](http://codyhouse.co/terms/)

Icons: [Nucleo](https://nucleoapp.com)
